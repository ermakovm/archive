package ru.mermakov.projects.MathLogic.Proof.Classes;

import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Main;
import ru.mermakov.projects.MathLogic.Parser.Classes.TokenHelper;

import java.util.ArrayList;
import java.util.Map;

public class ProofGenerator {
    protected Expression[] expressions;
    protected TokenHelper tokenHelper;

    public ProofGenerator(Expression[] expressions, TokenHelper tokenHelper) {
        this.expressions = expressions;
        this.tokenHelper = tokenHelper;
    }

    public ProofGenerator(String proofText, TokenHelper tokenHelper) {
        this(proofText.split("\n"), tokenHelper);
    }

    public int size(){
        return expressions.length;
    }

    public Expression get(int i){
        return expressions[i];
    }

    public Expression last(){
        return expressions[expressions.length-1];
    }

    public ProofGenerator(String[] lines, TokenHelper tokenHelper) {
        ArrayList<Expression> expressions = new ArrayList<Expression>(lines.length);
        for (String line : lines) {
            line = Main.removeComments(line).trim();
            if (!line.isEmpty()) {
                expressions.add(tokenHelper.getExpressionCompiler().compile(line));
            }
        }
        this.expressions = new Expression[expressions.size()];
        expressions.toArray(this.expressions);
    }

    public boolean addToProof(Proof proof, Map<String, Expression> substitution) {
        boolean failed = false;
        for (Expression expression : expressions) {
            failed |= (proof.addCheckTautology(expression.replaceVarsWithExpressions(substitution)) == null);
            if (failed) break;
        }
        return !failed;
    }
}
