package ru.mermakov.projects.MathLogic.Proof.Interfaces;

import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Expression.Classes.LogicalOperators.Implication;
import ru.mermakov.projects.MathLogic.Expression.Classes.SchemeAxioms;
import ru.mermakov.projects.MathLogic.Parser.Classes.TokenHelper;
import ru.mermakov.projects.MathLogic.Proof.Classes.Proof;

public interface AxiomSchemeList {
    public SchemeAxioms getAxiomScheme(int i);

    public int size();

    public boolean checkAxiomSchemeMatch(int i, Expression expression);

    public SchemeAxioms getMatchingAxiomScheme(Expression expression);

    public void addAssumptionImplicationProof(Proof proof, Expression A, Expression Ci);

    public void addMPImplicationProof(Proof proof, Expression A, Implication mpImplication);

    public void addSelfImplicationProof(Proof proof, Expression A);

    public void addTertiumNonDaturProof(Proof proof, Expression A);

    public void addAndOperatorProof(Proof proof, boolean leftOperandEvaluation,
                                    boolean rightOperandEvaluation,
                                    Expression leftOperand, Expression rightOperand);
    public void addOrOperatorProof(Proof proof, boolean leftOperandEvaluation,
                                   boolean rightOperandEvaluation,
                                   Expression leftOperand, Expression rightOperand);
    public void addImplicationOperatorProof(Proof proof, boolean leftOperandEvaluation,
                                            boolean rightOperandEvaluation,
                                            Expression leftOperand, Expression rightOperand);
    public void addNotOperatorProof(Proof proof, boolean operandEvaluation, Expression operand);

    public Proof mergeProofs(Proof A, Proof B);

    public TokenHelper getTokenHelper();
}
