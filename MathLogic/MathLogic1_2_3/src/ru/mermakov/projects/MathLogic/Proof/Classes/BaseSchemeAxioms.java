package ru.mermakov.projects.MathLogic.Proof.Classes;

import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Expression.Classes.SchemeAxioms;
import ru.mermakov.projects.MathLogic.Main;
import ru.mermakov.projects.MathLogic.Parser.Classes.AxiomTokenHelper;
import ru.mermakov.projects.MathLogic.Proof.Interfaces.AxiomSchemeList;

import java.util.ArrayList;

public abstract class BaseSchemeAxioms implements AxiomSchemeList {

    ArrayList<SchemeAxioms> axiomSchemes = new ArrayList<SchemeAxioms>();
    AxiomTokenHelper axiomTokenHolder = new AxiomTokenHelper();

    protected void initAxiomSchemes(String text) {
        initAxiomSchemes(text.split("\n"));
    }

    protected void initAxiomSchemes(String[] lines) {
        for (String line : lines) {
            line = Main.removeComments(line).trim();
            if (!line.isEmpty()) {
                addAxiomScheme(line);
            }
        }
    }

    protected void addAxiomScheme(SchemeAxioms axiom) {
        axiom.setId(axiomSchemes.size());
        axiomSchemes.add(axiom);
    }

    protected void addAxiomScheme(String source) {
        addAxiomScheme((SchemeAxioms) axiomTokenHolder.getExpressionCompiler().compile(source));
    }

    @Override
    public SchemeAxioms getAxiomScheme(int i) {
        return axiomSchemes.get(i);
    }

    @Override
    public int size() {
        return axiomSchemes.size();
    }

    @Override
    public boolean checkAxiomSchemeMatch(int i, Expression expression) {
        axiomTokenHolder.varMapping.clear();
        return axiomSchemes.get(i).checkStructureEquals(expression);
    }

    @Override
    public SchemeAxioms getMatchingAxiomScheme(Expression expression) {
        for (SchemeAxioms axiomScheme : axiomSchemes) {
            axiomTokenHolder.varMapping.clear();
            if (axiomScheme.checkStructureEquals(expression)) {
                return axiomScheme;
            }
        }
        return null;
    }

}
