package ru.mermakov.projects.MathLogic.Proof.Classes;

import ru.mermakov.projects.MathLogic.Exception.BracketsException;
import ru.mermakov.projects.MathLogic.Exception.CompileException;
import ru.mermakov.projects.MathLogic.Exception.UnlinkedTokensException;
import ru.mermakov.projects.MathLogic.Exception.WrongBracketsException;
import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Parser.Classes.Token;
import ru.mermakov.projects.MathLogic.Parser.Classes.TokenFinder;
import ru.mermakov.projects.MathLogic.Parser.Classes.TokenHelper;

import java.util.LinkedList;
import java.util.List;

public class ExpressionCompiler {
    private final TokenHelper tokenHelper;
    List<Token> tokens = null;
    int caret;
    int maxPriority;

    public ExpressionCompiler(TokenHelper tokenHelper) {
        this.tokenHelper = tokenHelper;

    }

    public Expression compile(String source) {
        TokenFinder tokenFinder = new TokenFinder(source, tokenHelper);
        tokens = tokenFinder.getTokens();
        maxPriority = -1;
        for (Token token : tokens) {
            int priority = token.type.getPriority();
            if (priority > maxPriority) maxPriority = priority;
        }
        caret = 0;
        tokens.add(new Token(new TokenHelper.ClosingBracketTokenType()));
        return recursiveDescentParseBrackets().expression;
    }

    protected Token recursiveDescentParseBrackets() {
        LinkedList<Token> tokens = new LinkedList<Token>();
        while (true) {
            if (caret >= this.tokens.size()) throw new WrongBracketsException();
            Token token = this.tokens.get(caret++);
            if (token.type.isClosingBracket()) {
                break;
            } else {
                if (token.type.isOpenningBracket()) {
                    tokens.add(recursiveDescentParseBrackets());
                } else {
                    tokens.add(token);
                }
            }
        }
        for (int priority = 0; priority <= maxPriority; ++priority) {
            for (int _i = 0; _i < tokens.size(); ++_i) {
                int i;
                if (TokenHelper.IS_RIGHT_ASSOC_LEVEL[priority]) {
                    i = tokens.size() - _i - 1;
                } else {
                    i = _i;
                }
                Token token = tokens.get(i);
                if (token.type.getPriority() == priority) {
                    Expression leftOperand = i == 0 ? null : tokens.get(i - 1).expression;
                    Expression rightOperand = i == tokens.size() - 1 ? null : tokens.get(i + 1).expression;
                    token.computeExpression(leftOperand, rightOperand);
                    if (rightOperand != null && token.type.isRightOperandUsed()) tokens.remove(i + 1);
                    if (leftOperand != null && token.type.isLeftOperandUsed()) tokens.remove(--i);
                }
                if (TokenHelper.IS_RIGHT_ASSOC_LEVEL[priority]) {
                    _i = tokens.size() - i - 1;
                } else {
                    _i = i;
                }
            }
        }
        if (tokens.isEmpty()) throw new BracketsException();
        if (tokens.size() > 1) throw new UnlinkedTokensException();
        if (tokens.getFirst().expression == null) throw new CompileException("Null expression resulted");
        return tokens.getFirst();
    }


}
