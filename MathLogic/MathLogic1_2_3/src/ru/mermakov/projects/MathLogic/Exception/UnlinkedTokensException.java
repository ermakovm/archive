package ru.mermakov.projects.MathLogic.Exception;

public class UnlinkedTokensException extends CalculationException {
    public UnlinkedTokensException() {
    }

    public UnlinkedTokensException(String detailMessage) {
        super(detailMessage);
    }

    public UnlinkedTokensException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UnlinkedTokensException(Throwable throwable) {
        super(throwable);
    }
}
