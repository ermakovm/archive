package ru.mermakov.projects.MathLogic.Exception;

public class WrongBracketsException extends CalculationException {
    public WrongBracketsException() {
    }

    public WrongBracketsException(String detailMessage) {
        super(detailMessage);
    }

    public WrongBracketsException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public WrongBracketsException(Throwable throwable) {
        super(throwable);
    }
}
