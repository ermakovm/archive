package ru.mermakov.projects.MathLogic.Exception;

public class UnsupportedComputationException extends CalculationException {
    public UnsupportedComputationException() {
    }

    public UnsupportedComputationException(String detailMessage) {
        super(detailMessage);
    }
}
