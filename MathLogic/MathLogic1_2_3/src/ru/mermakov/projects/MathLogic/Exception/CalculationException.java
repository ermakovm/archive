package ru.mermakov.projects.MathLogic.Exception;

public class CalculationException extends RuntimeException {
    public CalculationException() {
    }

    public CalculationException(String detailMessage) {
        super(detailMessage);
    }

    public CalculationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CalculationException(Throwable throwable) {
        super(throwable);
    }
}
