package ru.mermakov.projects.MathLogic.Exception;

public class CompileException extends CalculationException {
    public CompileException() {
    }

    public CompileException(String detailMessage) {
        super(detailMessage);
    }

    public CompileException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CompileException(Throwable throwable) {
        super(throwable);
    }
}
