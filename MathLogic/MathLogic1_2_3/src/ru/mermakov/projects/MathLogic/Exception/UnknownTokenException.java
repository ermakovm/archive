package ru.mermakov.projects.MathLogic.Exception;

public class UnknownTokenException extends CalculationException {
    public UnknownTokenException() {
    }

    public UnknownTokenException(String detailMessage) {
        super(detailMessage);
    }

    public UnknownTokenException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UnknownTokenException(Throwable throwable) {
        super(throwable);
    }
}
