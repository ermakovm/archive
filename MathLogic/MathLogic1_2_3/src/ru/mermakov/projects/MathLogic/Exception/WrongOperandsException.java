package ru.mermakov.projects.MathLogic.Exception;

public class WrongOperandsException extends CalculationException {
    public WrongOperandsException() {
    }

    public WrongOperandsException(String detailMessage) {
        super(detailMessage);
    }
}
