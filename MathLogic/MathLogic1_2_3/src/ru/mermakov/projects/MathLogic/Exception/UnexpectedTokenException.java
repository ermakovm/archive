package ru.mermakov.projects.MathLogic.Exception;

public class UnexpectedTokenException extends CalculationException {
    public UnexpectedTokenException() {
    }

    public UnexpectedTokenException(String detailMessage) {
        super(detailMessage);
    }

    public UnexpectedTokenException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UnexpectedTokenException(Throwable throwable) {
        super(throwable);
    }
}
