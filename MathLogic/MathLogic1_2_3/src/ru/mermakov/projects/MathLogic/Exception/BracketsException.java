package ru.mermakov.projects.MathLogic.Exception;

public class BracketsException extends CalculationException {
    public BracketsException() {
    }

    public BracketsException(String detailMessage) {
        super(detailMessage);
    }

    public BracketsException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public BracketsException(Throwable throwable) {
        super(throwable);
    }
}
