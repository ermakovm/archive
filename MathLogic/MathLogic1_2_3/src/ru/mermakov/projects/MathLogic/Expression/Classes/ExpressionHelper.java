package ru.mermakov.projects.MathLogic.Expression.Classes;

import ru.mermakov.projects.MathLogic.Expression.Classes.LogicalOperators.LogicalAnd;
import ru.mermakov.projects.MathLogic.Expression.Classes.LogicalOperators.Implication;
import ru.mermakov.projects.MathLogic.Expression.Classes.LogicalOperators.LogicalNot;
import ru.mermakov.projects.MathLogic.Expression.Classes.LogicalOperators.LogicalOr;

import java.util.WeakHashMap;

public class ExpressionHelper {
    private ExpressionHelper(){}
    private static ExpressionHelper _instance = null;
    public static ExpressionHelper instance(){
        return _instance==null?(_instance = new ExpressionHelper()): _instance;
    }

    WeakHashMap<Expression, Expression> expressions = new WeakHashMap<Expression, Expression>();

    public LogicalAnd getAndExpression(Expression leftOperand, Expression rightOperand){
        LogicalAnd newObject = new LogicalAnd(leftOperand, rightOperand);
        LogicalAnd oldObject = (LogicalAnd) expressions.get(newObject);
        if(oldObject != null) return oldObject;
        else expressions.put(newObject, newObject);
        return newObject;
    }
    public LogicalNot getNotExpression(Expression operand){
        LogicalNot newObject = new LogicalNot(operand);
        LogicalNot oldObject = (LogicalNot) expressions.get(newObject);
        if(oldObject != null) return oldObject;
        else expressions.put(newObject, newObject);
        return newObject;
    }
    public Variable getVariableExpression(String varName){
        Variable newObject = new Variable(varName);
        Variable oldObject = (Variable) expressions.get(newObject);
        if(oldObject != null) return oldObject;
        else expressions.put(newObject, newObject);
        return newObject;
    }

    public Implication getImplicationExpression(Expression leftOperand, Expression rightOperand){
        Implication newObject = new Implication(leftOperand, rightOperand);
        Implication oldObject = (Implication) expressions.get(newObject);
        if(oldObject != null) return oldObject;
        else expressions.put(newObject, newObject);
        return newObject;
    }
    public LogicalOr getOrExpression(Expression leftOperand, Expression rightOperand){
        LogicalOr newObject = new LogicalOr(leftOperand, rightOperand);
        LogicalOr oldObject = (LogicalOr) expressions.get(newObject);
        if(oldObject != null) return oldObject;
        else expressions.put(newObject, newObject);
        return newObject;
    }
}
