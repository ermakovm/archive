package ru.mermakov.projects.MathLogic.Expression.Classes.LogicalOperators;

import ru.mermakov.projects.MathLogic.Expression.Classes.BinOp;
import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Expression.Classes.ExpressionHelper;
import ru.mermakov.projects.MathLogic.Expression.Interfaces.Consts;
import ru.mermakov.projects.MathLogic.Main;
import ru.mermakov.projects.MathLogic.Proof.Classes.Proof;

public class LogicalAnd extends BinOp implements Consts{
    public LogicalAnd(Expression leftOperand, Expression rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected int getClassUniqueId() {
        return 0x75abc32f;
    }

    @Override
    protected String getOperationStringRepresentation() {
        return Main.ALT_PRINT_MODE ? ' ' + Consts.AND_OPERATION_ALT + ' ' : Consts.AND_OPERATION;
    }

    @Override
    protected boolean evaluateImpl(boolean left, boolean right) {
        return left & right;
    }

    @Override
    protected Expression createNewInstance(Expression leftOperand, Expression rightOperand) {
        return ExpressionHelper.instance().getAndExpression(leftOperand, rightOperand);
    }

    @Override
    protected void proveExpressionImpl(Proof proof, boolean leftOperandEvaluation, boolean rightOperandEvaluation, Expression leftOperand, Expression rightOperand) {
        proof.getAxiomSchemeList().addAndOperatorProof(proof, leftOperandEvaluation, rightOperandEvaluation, leftOperand, rightOperand);
    }
}
