package ru.mermakov.projects.MathLogic.Expression.Classes.LogicalOperators;

import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Expression.Classes.ExpressionHelper;
import ru.mermakov.projects.MathLogic.Expression.Classes.UnaryOperator;
import ru.mermakov.projects.MathLogic.Expression.Interfaces.Consts;
import ru.mermakov.projects.MathLogic.Main;
import ru.mermakov.projects.MathLogic.Proof.Classes.Proof;

public class LogicalNot extends UnaryOperator implements Consts {
    public LogicalNot(Expression operand) {
        super(operand);
    }

    @Override
    protected int getClassUniqueId() {
        return 0x5772abdf;
    }
    @Override
    protected String getOperationStringRepresentation() {
        return Main.ALT_PRINT_MODE ? Consts.NOT_OPERATION_ALT : Consts.NOT_OPERATION;
    }

    @Override
    protected boolean evaluateImpl(boolean value) {
        return !value;
    }


    @Override
    protected Expression createNewInstance(Expression operand) {
        return ExpressionHelper.instance().getNotExpression(operand);
    }

    @Override
    protected void proveExpressionImpl(Proof proof, boolean operandEvaluationValue, Expression operand) {
        proof.getAxiomSchemeList().addNotOperatorProof(proof, operandEvaluationValue, operand);
    }
}
