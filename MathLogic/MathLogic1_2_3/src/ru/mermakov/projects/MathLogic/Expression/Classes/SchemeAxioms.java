package ru.mermakov.projects.MathLogic.Expression.Classes;

import ru.mermakov.projects.MathLogic.Proof.Classes.Proof;

import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

public class SchemeAxioms extends Expression {
    int id;

    @Override
    protected Expression negateImpl() {
        throw new UnsupportedOperationException("Axiom scheme expressions isn't meant to be used with negate facility");
    }

    @Override
    public Expression replaceVarsWithExpressions(Map<String, Expression> substitution) {
        throw new UnsupportedOperationException("Axiom scheme expressions isn't meant to be used with replaceVarsWithExpressions facility");
    }

    @Override
    public void proveExpression(Proof proof, Map<String, Boolean> variableMapping) {
        throw new UnsupportedOperationException("Axiom scheme expressions isn't meant to be used with proveExpression facility");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    Expression expression;
    Map<String, Expression> varMapping;

    public SchemeAxioms(Expression expression, Map<String, Expression> varMapping) {
        this.varMapping = varMapping;
        this.expression = expression;
    }

    public boolean checkStructureEquals(Expression expr) {
        if (expression instanceof Variable) {
            String varName = expression.toString();
            if (varMapping.containsKey(varName)) {
                return varMapping.get(varName).equals(expr);
            } else {
                varMapping.put(varName, expr);
                return true;
            }
        } else {
            if (expr.getClass() != expression.getClass()) return false;
            if (expression instanceof UnaryOperator) {
                SchemeAxioms axiomOperand = (SchemeAxioms) ((UnaryOperator) expression).operand;
                Expression operand = ((UnaryOperator) expr).operand;
                return axiomOperand.checkStructureEquals(operand);
            } else if (expression instanceof BinOp) {
                SchemeAxioms axiomLeftOperand = (SchemeAxioms) ((BinOp) expression).leftOperand;
                SchemeAxioms axiomRightOperand = (SchemeAxioms) ((BinOp) expression).rightOperand;
                Expression leftOperand = ((BinOp) expr).leftOperand;
                Expression rightOperand = ((BinOp) expr).rightOperand;
                return axiomLeftOperand.checkStructureEquals(leftOperand) && axiomRightOperand.checkStructureEquals(rightOperand);
            }
        }
        return false;
    }

    @Override
    public void digVariables(Set<String> variableHolder) {
        expression.digVariables(variableHolder);
    }

    @Override
    public boolean evaluate(Map<String, Boolean> variableMapping) {
        return expression.evaluate(variableMapping);
    }

    @Override
    public String toString() {
        return expression.toString();
    }

    @Override
    public void appendToStringBuilder(StringBuilder sb) {
        expression.appendToStringBuilder(sb);
    }

    @Override
    public void printToPrintWriter(PrintWriter printWriter) {
        expression.printToPrintWriter(printWriter);
    }

    @Override
    public boolean equals(Object o) {
        if(o==null) return false;
        if(o.hashCode()!=hashCode()) return false;
        if (this == o) return true;
        if (!(o instanceof SchemeAxioms)) return false;
        if (!super.equals(o)) return false;

        SchemeAxioms that = (SchemeAxioms) o;

        if (!expression.equals(that.expression)) return false;

        return true;
    }

    @Override
    public int hashCodeImpl() {
        return expression.hashCode()^0x4732dbfc;
    }
}
