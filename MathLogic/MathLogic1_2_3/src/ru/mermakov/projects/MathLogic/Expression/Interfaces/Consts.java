package ru.mermakov.projects.MathLogic.Expression.Interfaces;

public interface Consts {
    public static final String AND_OPERATION = "&";
    public static final String OR_OPERATION = "|";
    public static final String IMPLICATION_OPERATION = "->";
    public static final String NOT_OPERATION = "!";
    public static final String AND_OPERATION_ALT = "∧";
    public static final String OR_OPERATION_ALT = "∨";
    public static final String IMPLICATION_OPERATION_ALT = "→";
    public static final String NOT_OPERATION_ALT = "¬";
}
