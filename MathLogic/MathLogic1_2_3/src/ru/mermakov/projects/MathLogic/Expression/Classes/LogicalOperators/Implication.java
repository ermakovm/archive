package ru.mermakov.projects.MathLogic.Expression.Classes.LogicalOperators;

import ru.mermakov.projects.MathLogic.Expression.Classes.BinOp;
import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Expression.Classes.ExpressionHelper;
import ru.mermakov.projects.MathLogic.Expression.Interfaces.Consts;
import ru.mermakov.projects.MathLogic.Main;
import ru.mermakov.projects.MathLogic.Proof.Classes.Proof;


public class Implication extends BinOp implements Consts{
    public Implication(Expression leftOperand, Expression rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected int getClassUniqueId() {
        return 0x9a5f72c5;
    }
    @Override
    protected String getOperationStringRepresentation() {
        return Main.ALT_PRINT_MODE ? ' ' + Consts.IMPLICATION_OPERATION_ALT + ' ' : Consts.IMPLICATION_OPERATION;
    }

    @Override
    protected boolean evaluateImpl(boolean left, boolean right) {
        return !left | (left & right);
    }

    @Override
    protected Expression createNewInstance(Expression leftOperand, Expression rightOperand) {
        return ExpressionHelper.instance().getImplicationExpression(leftOperand, rightOperand);
    }

    @Override
    protected void proveExpressionImpl(Proof proof, boolean leftOperandEvaluation, boolean rightOperandEvaluation, Expression leftOperand, Expression rightOperand) {
        proof.getAxiomSchemeList().addImplicationOperatorProof(proof, leftOperandEvaluation, rightOperandEvaluation, leftOperand, rightOperand);
    }
}
