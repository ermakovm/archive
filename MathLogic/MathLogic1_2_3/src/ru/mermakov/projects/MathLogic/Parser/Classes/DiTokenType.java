package ru.mermakov.projects.MathLogic.Parser.Classes;

import ru.mermakov.projects.MathLogic.Exception.WrongOperandsException;
import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Parser.Interfaces.TokenType;

public abstract class DiTokenType implements TokenType {
    @Override
    public Expression getExpression(Expression leftOperand, Expression rightOperand) {
        if (leftOperand == null) {
            throw new WrongOperandsException("Left operand passed null for both assoc operator: " + getClass().toString());
        }
        if (rightOperand == null) {
            throw new WrongOperandsException("Right operand passed null for both assoc operator: " + getClass().toString());
        }
        return getExpressionImpl(leftOperand, rightOperand);
    }

    protected abstract Expression getExpressionImpl(Expression leftOperand, Expression rightOperand);

    @Override
    public boolean isLeftOperandUsed() {
        return true;
    }

    @Override
    public boolean isRightOperandUsed() {
        return true;
    }

    @Override
    public boolean isOpenningBracket() {
        return false;
    }

    @Override
    public boolean isClosingBracket() {
        return false;
    }
}
