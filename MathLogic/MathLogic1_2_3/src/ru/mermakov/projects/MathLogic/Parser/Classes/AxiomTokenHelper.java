package ru.mermakov.projects.MathLogic.Parser.Classes;

import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Expression.Classes.SchemeAxioms;
import ru.mermakov.projects.MathLogic.Parser.Interfaces.BTokenType;
import ru.mermakov.projects.MathLogic.Parser.Interfaces.ManualTokenType;
import ru.mermakov.projects.MathLogic.Parser.Interfaces.TokenType;

import java.util.HashMap;

public class AxiomTokenHelper extends TokenHelper {

    public final HashMap<String, Expression> varMapping = new HashMap<String, Expression>();


    @Override
    protected ManualTokenType[] generateManualTokenTypeFabrics() {
        final ManualTokenType[] _factories = super.generateManualTokenTypeFabrics();
        ManualTokenType[] factories = new ManualTokenType[_factories.length];
        for (int i = 0; i < factories.length; ++i) {
            final ManualTokenType factory = _factories[i];
            factories[i] = new ManualTokenType() {
                @Override
                public TokenType getTokenType(final String part) {
                    return new TokenType() {
                        @Override
                        public boolean isOpenningBracket() {
                            return parent.isOpenningBracket();  //To change body of implemented methods use File | Settings | File Templates.
                        }

                        @Override
                        public boolean isClosingBracket() {
                            return parent.isClosingBracket();  //To change body of implemented methods use File | Settings | File Templates.
                        }

                        TokenType parent = factory.getTokenType(part);

                        @Override
                        public Expression getExpression(Expression leftOperand, Expression rightOperand) {
                            return new SchemeAxioms(parent.getExpression(leftOperand, rightOperand), varMapping);
                        }

                        @Override
                        public boolean isLeftOperandUsed() {
                            return parent.isLeftOperandUsed();  //To change body of implemented methods use File | Settings | File Templates.
                        }

                        @Override
                        public boolean isRightOperandUsed() {
                            return parent.isRightOperandUsed();  //To change body of implemented methods use File | Settings | File Templates.
                        }

                        @Override
                        public int getPriority() {
                            return parent.getPriority();  //To change body of implemented methods use File | Settings | File Templates.
                        }
                    };
                }
            };
        }
        return factories;
    }

    @Override
    protected BTokenType[] generateAhoTokenTypes() {
        BTokenType[] _bTokenTypes = super.generateAhoTokenTypes();
        BTokenType[] bTokenTypes = new BTokenType[_bTokenTypes.length];
        for (int i = 0; i < _bTokenTypes.length; ++i) {
            final BTokenType bTokenType = _bTokenTypes[i];
            bTokenTypes[i] = new BAbstractTokenType() {
                @Override
                public String getMatchString() {
                    return bTokenType.getMatchString();
                }

                @Override
                public Expression getExpression(Expression leftOperand, Expression rightOperand) {
                    return new SchemeAxioms(bTokenType.getExpression(leftOperand, rightOperand), varMapping);
                }

                @Override
                public boolean isLeftOperandUsed() {
                    return bTokenType.isLeftOperandUsed();
                }

                @Override
                public boolean isRightOperandUsed() {
                    return bTokenType.isRightOperandUsed();
                }

                @Override
                public int getPriority() {
                    return bTokenType.getPriority();
                }

                @Override
                public boolean isOpenningBracket() {
                    return bTokenType.isOpenningBracket();
                }

                @Override
                public boolean isClosingBracket() {
                    return bTokenType.isClosingBracket();
                }
            };
        }
        return bTokenTypes;
    }
}
