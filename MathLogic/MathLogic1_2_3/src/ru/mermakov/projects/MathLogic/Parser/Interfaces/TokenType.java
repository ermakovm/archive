package ru.mermakov.projects.MathLogic.Parser.Interfaces;

import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;

public interface TokenType {
    public Expression getExpression(Expression leftOperand, Expression rightOperand);

    public boolean isLeftOperandUsed();

    public boolean isRightOperandUsed();

    public int getPriority();

    public boolean isOpenningBracket();

    public boolean isClosingBracket();
}
