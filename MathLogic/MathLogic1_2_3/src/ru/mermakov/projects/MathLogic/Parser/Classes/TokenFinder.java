package ru.mermakov.projects.MathLogic.Parser.Classes;

import ru.mermakov.projects.MathLogic.Exception.UnknownTokenException;
import ru.mermakov.projects.MathLogic.Parser.Interfaces.BTokenType;
import ru.mermakov.projects.MathLogic.Parser.Interfaces.ManualTokenType;
import ru.mermakov.projects.MathLogic.Parser.Interfaces.TokenType;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TokenFinder {

    public final TokenType[] ahoTokenTypes;
    final protected char[] expressionChars;
    final protected String expression;
    protected LinkedList<Token> tokens = new LinkedList<Token>();
    protected Trie<TokenType>.Pair[] pos;
    protected ManualTokenType[] manualTokentTypesFabrics;
    protected Trie<TokenType> trie = new Trie();

    public TokenFinder(String expression, TokenHelper tokenHelper) {
        this.expression = expression;
        expressionChars = expression.toCharArray();
        pos = new Trie.Pair[expressionChars.length];
        ahoTokenTypes = tokenHelper.getbTokenTypes();
        manualTokentTypesFabrics = tokenHelper.getManualTokenTypeFabrics();
    }

    protected void init() {
        for (TokenType tokenType : ahoTokenTypes) {
            if (tokenType instanceof BTokenType) {
                String word = ((BTokenType) tokenType).getMatchString();
                trie.addWord(word, tokenType);
            }
        }
    }

    protected void processAho() {
        Trie.TrieNode node = trie.root;
        for (int i = 0; i < expressionChars.length; ++i) {
            node = node.go(expressionChars[i]);
            Trie<TokenType>.Pair pair = node.getLongestMatchingPair();
            pos[i] = pair;
        }
    }

    protected TokenType getManual(String part) {
        part = part.trim();
        if (part.isEmpty()) return null;
        TokenType tokenType;
        for (ManualTokenType factory : manualTokentTypesFabrics) {
            if ((tokenType = factory.getTokenType(part)) != null) return tokenType;
        }
        throw new UnknownTokenException("Unknown token: " + part);
    }

    protected void computeTokens() {
        for (int j = expressionChars.length - 1; j >= 0; --j) {
            int i;
            if (pos[j] != null) {
                i = j + 1 - pos[j].word.length();
                tokens.add(new Token(pos[j].data));
            } else {
                i = j;
                while (i > 0 && pos[i - 1] == null) --i;
                String manualPart = expression.substring(i, j + 1);
                TokenType manual = getManual(manualPart);
                if (manual != null) tokens.add(new Token(manual));
            }
            j = i;
        }
        Collections.reverse(tokens);
    }

    public List<Token> getTokens() {
        if (tokens != null) {
            init();
            processAho();
            computeTokens();
        }
        return tokens;
    }


}
