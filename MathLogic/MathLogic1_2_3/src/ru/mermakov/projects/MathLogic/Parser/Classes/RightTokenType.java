package ru.mermakov.projects.MathLogic.Parser.Classes;

import ru.mermakov.projects.MathLogic.Exception.WrongOperandsException;
import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Parser.Interfaces.TokenType;

public abstract class RightTokenType implements TokenType {
    @Override
    public Expression getExpression(Expression leftOperand, Expression rightOperand) {
        if (rightOperand == null) {
            throw new WrongOperandsException("Right operand passed null for right assoc operator: " + getClass().toString());
        }
        return getExpressionImpl(rightOperand);
    }

    protected abstract Expression getExpressionImpl(Expression rightOperand);

    @Override
    public boolean isLeftOperandUsed() {
        return false;
    }

    @Override
    public boolean isRightOperandUsed() {
        return true;
    }

    @Override
    public boolean isOpenningBracket() {
        return false;
    }

    @Override
    public boolean isClosingBracket() {
        return false;
    }
}
