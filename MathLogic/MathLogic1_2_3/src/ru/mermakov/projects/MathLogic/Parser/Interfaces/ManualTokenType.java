package ru.mermakov.projects.MathLogic.Parser.Interfaces;

public interface ManualTokenType {
    public TokenType getTokenType(String part);
}
