package ru.mermakov.projects.MathLogic.Parser.Classes;

import ru.mermakov.projects.MathLogic.Expression.Classes.Expression;
import ru.mermakov.projects.MathLogic.Parser.Interfaces.TokenType;

public class Token {
    public TokenType type = null;
    public Expression expression = null;

    public Token(TokenType type) {
        this.type = type;
    }

    public Expression computeExpression(Expression leftOperand, Expression rightOperand) {
        if (expression == null) {
            expression = type.getExpression(leftOperand, rightOperand);
        }
        return expression;
    }

}
