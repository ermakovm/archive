package ru.mermakov.projects.MathLogic.Parser.Interfaces;

public interface BTokenType extends TokenType {
    public String getMatchString();
}
