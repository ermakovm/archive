package info.mermakov.projects.ifmo.mathlogic.expression.logical.binary;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.LogicalExpression;

import java.util.Set;

public abstract class BinaryLogicalExpression extends LogicalExpression {
    public LogicalExpression left, right;

    public BinaryLogicalExpression(LogicalExpression left, LogicalExpression right) {
        this.left = left;
        this.right = right;
    }

    protected boolean isFinished() {
        return left != null;
    }

    protected void setArgs(LogicalExpression left, LogicalExpression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Set<Variable> getFreeVariables(Set<Variable> bounded) {
        Set<Variable> free = left.getFreeVariables(bounded);
        free.addAll(right.getFreeVariables(bounded));
        return free;
    }

    @Override
    public boolean bounds(Variable v, Set<Variable> free, Set<Variable> bounded) {
        return left.bounds(v, free, bounded) || right.bounds(v, free, bounded);
    }

    @Override
    public FunctionalExpression tryGetSubstitution(Variable v, Expression expression) {
        if (expression instanceof BinaryLogicalExpression) {
            FunctionalExpression result = left.tryGetSubstitution(v, ((BinaryLogicalExpression) expression).left);
            if (result == null) {
                return right.tryGetSubstitution(v, ((BinaryLogicalExpression) expression).right);
            }
            return result;
        }
        return null;
    }
}
