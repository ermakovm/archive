package info.mermakov.projects.ifmo.mathlogic.expression.functional;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Function extends FunctionalExpression {
    private String name;
    private List<FunctionalExpression> args;

    public Function(String name, List<FunctionalExpression> args) {
        this.name = name;
        this.args = args;
    }

    protected void setArgs(List<FunctionalExpression> args) {
        this.args = args;
    }

    protected boolean isFinished() {
        return args != null;
    }

    protected String getName() {
        return name;
    }

    @Override
    public Function substitute(Variable v, FunctionalExpression expression) {
        List<FunctionalExpression> newArgs = args.stream().map(fe -> fe.substitute(v, expression)).collect(Collectors.toList());
        return new Function(name, newArgs);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Function && name.equals(((Function) o).name) && args.size() == ((Function) o).args.size()) {
            for (int i = 0; i < args.size(); i++) {
                if (!args.get(i).equals(((Function) o).args.get(i))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hashCode = name.hashCode();
        for (int i = 0; i < args.size(); i++) {
            hashCode += (i % 2 == 0 ? -1 : 1) * (i + 29) * args.get(i).hashCode();
        }
        return hashCode;
    }

    @Override
    public Set<Variable> getFreeVariables(Set<Variable> bounded) {
        Set<Variable> free = new HashSet<>();
        for (FunctionalExpression fe : args) {
            free.addAll(fe.getFreeVariables(bounded));
        }
        return free;
    }

    @Override
    public boolean bounds(Variable v, Set<Variable> free, Set<Variable> bounded) {
        for (FunctionalExpression fe : args) {
            if (fe.bounds(v, free, bounded)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public FunctionalExpression tryGetSubstitution(Variable v, Expression expression) {
        if (expression instanceof Function) {
            if (args.size() != ((Function) expression).args.size()) {
                return null;
            }
            for (int i = 0; i < args.size(); i++) {
                FunctionalExpression result = args.get(i).tryGetSubstitution(v, ((Function) expression).args.get(i));
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String res = name + "(";
        for (int i = 0; i < args.size(); i++) {
            res += args.get(i) + (i + 1 == args.size() ? "" : ",");
        }
        return res + ")";
    }
}
