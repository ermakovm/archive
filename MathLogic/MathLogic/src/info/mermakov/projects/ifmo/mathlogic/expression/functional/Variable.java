package info.mermakov.projects.ifmo.mathlogic.expression.functional;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;

import java.util.HashSet;
import java.util.Set;

public class Variable extends FunctionalExpression {
    private String name;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public FunctionalExpression substitute(Variable v, FunctionalExpression expression) {
        return equals(v) ? expression : this;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Variable && name.equals(((Variable) o).name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public Set<Variable> getFreeVariables(Set<Variable> bounded) {
        Set<Variable> free = new HashSet<>();
        if (!bounded.contains(this)) {
            free.add(this);
        }
        return free;
    }

    @Override
    public boolean bounds(Variable v, Set<Variable> free, Set<Variable> bounded) {
        if (equals(v)) {
            for (Variable variable : free) {
                if (bounded.contains(variable)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public FunctionalExpression tryGetSubstitution(Variable v, Expression expression) {
        if (expression instanceof FunctionalExpression && equals(v)) {
            return (FunctionalExpression) expression;
        }
        return null;
    }

    @Override
    public String toString() {
        return name;
    }
}