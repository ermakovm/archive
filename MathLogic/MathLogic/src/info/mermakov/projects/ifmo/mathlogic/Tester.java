package info.mermakov.projects.ifmo.mathlogic;

import info.mermakov.projects.ifmo.mathlogic.helper.Solver;

import java.io.IOException;

public class Tester {
    public static void main(String[] args) throws IOException {
        Solver.solve(true, "tests/4/tasks/axiom.in", "tests/4/ans/axiom.out");
        Solver.solve(true, "tests/4/tasks/intro.in", "tests/4/ans/intro.out");
        Solver.solve(true, "tests/4/tasks/intro2.in", "tests/4/ans/intro2.out");
        Solver.solve(true, "tests/4/tasks/must-fail.in", "tests/4/ans/must-fail.out");
        Solver.solve(true, "tests/4/tasks/must-fail2.in", "tests/4/ans/must-fail2.out");
        Solver.solve(true, "tests/4/tasks/must-fail3.in", "tests/4/ans/must-fail3.out");

        Solver.solve(false, "tests/5/tasks/must-fail.in", "tests/5/ans/must-fail.out");
        Solver.solve(false, "tests/5/tasks/induction.in", "tests/5/ans/induction.out");
        Solver.solve(false, "tests/5/tasks/reflexive.in", "tests/5/ans/reflexive.out");
    }
}