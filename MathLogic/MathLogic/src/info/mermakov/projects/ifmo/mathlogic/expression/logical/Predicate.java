package info.mermakov.projects.ifmo.mathlogic.expression.logical;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Predicate extends LogicalExpression {
    private String name;
    private List<FunctionalExpression> args;

    public Predicate(String name, List<FunctionalExpression> args) {
        this.name = name;
        this.args = args;
    }

    @Override
    public Predicate substitute(Variable v, FunctionalExpression expression) {
        List<FunctionalExpression> newArgs = args.stream().map(fe -> fe.substitute(v, expression)).collect(Collectors.toList());
        return new Predicate(name, newArgs);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Predicate && name.equals(((Predicate) o).name) && args.size() == ((Predicate) o).args.size()) {
            for (int i = 0; i < args.size(); i++) {
                if (!args.get(i).equals(((Predicate) o).args.get(i))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hashCode = name.hashCode();
        for (int i = 0; i < args.size(); i++) {
            hashCode += i * args.get(i).hashCode();
        }
        return hashCode;
    }

    @Override
    public Set<Variable> getFreeVariables(Set<Variable> bounded) {
        Set<Variable> free = new HashSet<>();
        for (FunctionalExpression fe : args) {
            free.addAll(fe.getFreeVariables(bounded));
        }
        return free;
    }

    @Override
    public boolean bounds(Variable v, Set<Variable> free, Set<Variable> bounded) {
        for (FunctionalExpression fe : args) {
            if (fe.bounds(v, free, bounded)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isTemplateFor(LogicalExpression expression, List<LogicalExpression> state) {
        return false;
    }

    @Override
    public FunctionalExpression tryGetSubstitution(Variable v, Expression expression) {
        if (expression instanceof Predicate) {
            for (int i = 0; i < args.size(); i++) {
                FunctionalExpression result = args.get(i).tryGetSubstitution(v, ((Predicate) expression).args.get(i));
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String res = name + "(";
        for (int i = 0; i < args.size(); i++) {
            res += args.get(i) + (i + 1 == args.size() ? "" : ",");
        }
        return res + ")";
    }
}