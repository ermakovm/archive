package info.mermakov.projects.ifmo.mathlogic.expression.logical;

import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;

public class ForAllQuantifier extends Quantifier {
    public ForAllQuantifier(Variable variable, LogicalExpression expression) {
        super(variable, expression);
    }

    @Override
    public ForAllQuantifier substitute(Variable v, FunctionalExpression fe) {
        return v.equals(variable) ? this : new ForAllQuantifier(variable, expression.substitute(v, fe));
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ForAllQuantifier && variable.equals(((ForAllQuantifier) o).variable) && expression.equals(((ForAllQuantifier) o).expression);
    }

    @Override
    public int hashCode() {
        return variable.hashCode() - 5 * expression.hashCode() + 7;
    }

    @Override
    public String toString() {
        return "(@" + variable + expression + ")";
    }
}
