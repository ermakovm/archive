package info.mermakov.projects.ifmo.mathlogic.expression.logical;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Function;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.binary.And;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.binary.Implication;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.binary.Or;

import java.util.ArrayList;
import java.util.List;

public abstract class LogicalExpression extends Expression {
    public static String error = "";

    public boolean isAxiom() {
        error = "";
        if (new Implication(new Template(0), new Implication(new Template(1), new Template(0))).isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        if (new Implication(new Implication(new Template(0), new Template(1)), new Implication(new Implication(new Template(0), new Implication(new Template(1), new Template(2))), new Implication(new Template(0), new Template(2)))).isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        if (new Implication(new Template(0), new Implication(new Template(1), new And(new Template(0), new Template(1)))).isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        if (new Implication(new And(new Template(0), new Template(1)), new Template(0)).isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        if (new Implication(new And(new Template(0), new Template(1)), new Template(1)).isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        if (new Implication(new Template(0), new Or(new Template(0), new Template(1))).isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        if (new Implication(new Template(1), new Or(new Template(0), new Template(1))).isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        if (new Implication(new Implication(new Template(0), new Template(2)), new Implication(new Implication(new Template(1), new Template(2)), new Implication(new Or(new Template(0), new Template(1)), new Template(2)))).isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        LogicalExpression e = new Implication(new Implication(new Template(0), new Template(1)), new Implication(new Implication(new Template(0), new Not(new Template(1))), new Not(new Template(0))));
        if (e.isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        if (new Implication(new Not(new Not(new Template(0))), new Template(0)).isTemplateFor(this, new ArrayList<>())) {
            return true;
        }
        if (this instanceof Implication) {
            LogicalExpression left = ((Implication) this).left, right = ((Implication) this).right;
            if (left instanceof ForAllQuantifier) {
                Variable v = ((ForAllQuantifier) left).variable;
                LogicalExpression expression = ((ForAllQuantifier) left).expression;
                if (right.isSubstitution(v, expression)) {
                    if (expression.isFreeForSubstitution(v, expression.tryGetSubstitution(v, right))) {
                        return true;
                    } else {
                        error = expression.tryGetSubstitution(v, right) + " is not free for substitution as " + v + " in " + expression;
                    }
                }
            }
            if (right instanceof ExistsQuantifier) {
                Variable v = ((ExistsQuantifier) right).variable;
                LogicalExpression expression = ((ExistsQuantifier) right).expression;
                if (left.isSubstitution(v, expression)) {
                    if (expression.isFreeForSubstitution(v, expression.tryGetSubstitution(v, left))) {
                        return true;
                    } else {
                        error = expression.tryGetSubstitution(v, left) + " is not free for substitution as " + v + " in " + expression;
                    }
                }
            }
            if (left instanceof And) {
                LogicalExpression expression = right;
                right = ((And) left).right;
                left = ((And) left).left;
                if (right instanceof ForAllQuantifier) {
                    Variable v = ((ForAllQuantifier) right).variable;
                    LogicalExpression qe = ((ForAllQuantifier) right).expression;
                    if (left.equals(expression.substitute(v, new Function("0", new ArrayList<>()))) && qe instanceof Implication) {
                        left = ((Implication) qe).left;
                        right = ((Implication) qe).right;
                        List<FunctionalExpression> args = new ArrayList<>();
                        args.add(v);
                        if (left.equals(expression) && right.equals(expression.substitute(v, new Function("'", args)))) {
                            return true;
                        }
                    }
                }
            }
        }
        if (equals(Expression.parseExpression("a=b->a'=b'"))) {
            return true;
        }
        if (equals(Expression.parseExpression("a=b->a=c->b=c"))) {
            return true;
        }
        if (equals(Expression.parseExpression("a'=b'->a=b"))) {
            return true;
        }
        if (equals(Expression.parseExpression("!a'=0"))) {
            return true;
        }
        if (equals(Expression.parseExpression("a+b'=(a+b)'"))) {
            return true;
        }
        if (equals(Expression.parseExpression("a+0=a"))) {
            return true;
        }
        if (equals(Expression.parseExpression("a*0=0"))) {
            return true;
        }
        if (equals(Expression.parseExpression("a*b'=a*b+a"))) {
            return true;
        }
        return false;
    }

    @Override
    public abstract LogicalExpression substitute(Variable v, FunctionalExpression expression);

    public abstract boolean isTemplateFor(LogicalExpression expression, List<LogicalExpression> state);
}
