package info.mermakov.projects.ifmo.mathlogic;

import info.mermakov.projects.ifmo.mathlogic.helper.Solver;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length < 6) {
            System.out.println("Usage:");
            System.out.println("-t N, where N = 4 or 5; -in FileName; -out FileName");
            return;
        }
        boolean task = false;
        String fileIn = "", fileOut = "";
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-t")) {
                if (args[i + 1].equals("5"))
                    task = false;
                if (args[i + 1].equals("4"))
                    task = true;
                i++;
            }
            if (args[i].equals("-in")) {
                fileIn = args[i + 1];
                i++;
            }
            if (args[i].equals("-out")) {
                fileOut = args[i + 1];
            }
        }
        Solver.solve(task, fileIn, fileOut);
    }
}
