package info.mermakov.projects.ifmo.mathlogic.expression.logical;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;

import java.util.List;
import java.util.Set;

public abstract class Quantifier extends LogicalExpression {
    public Variable variable;
    public LogicalExpression expression;

    public Quantifier(Variable variable, LogicalExpression expression) {
        this.variable = variable;
        this.expression = expression;
    }

    @Override
    public Set<Variable> getFreeVariables(Set<Variable> bounded) {
        boolean isBounded = bounded.contains(variable);
        if (!isBounded) {
            bounded.add(variable);
        }
        Set<Variable> free = expression.getFreeVariables(bounded);
        if (!isBounded) {
            bounded.remove(variable);
        }
        return free;
    }

    @Override
    public boolean bounds(Variable v, Set<Variable> free, Set<Variable> bounded) {
        if (v.equals(variable)) {
            return false;
        }
        boolean isBounded = bounded.contains(variable);
        if (!isBounded) {
            bounded.add(variable);
        }
        boolean result = expression.bounds(v, free, bounded);
        if (!isBounded) {
            bounded.remove(variable);
        }
        return result;
    }

    @Override
    public FunctionalExpression tryGetSubstitution(Variable v, Expression expr) {
        if (expr instanceof Quantifier && !variable.equals(v)) {
            return expression.tryGetSubstitution(v, ((Quantifier) expr).expression);
        }
        return null;
    }

    @Override
    public boolean isTemplateFor(LogicalExpression expression, List<LogicalExpression> state) {
        return false;
    }
}
