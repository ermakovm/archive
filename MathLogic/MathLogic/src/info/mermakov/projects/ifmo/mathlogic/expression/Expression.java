package info.mermakov.projects.ifmo.mathlogic.expression;

import info.mermakov.projects.ifmo.mathlogic.expression.functional.Function;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.*;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.binary.And;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.binary.Implication;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.binary.Or;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class Expression {
    public static LogicalExpression parseExpression(String s) {
        return Parser.parseExpression(Lexer.getLexemes(s));
    }

    public boolean isSubstitution(Variable v, Expression e) {
        return equals(e.substitute(v, e.tryGetSubstitution(v, this)));
    }

    public abstract Expression substitute(Variable v, FunctionalExpression expression);

    public boolean isFreeForSubstitution(Variable v, FunctionalExpression expression) {
        return !bounds(v, expression.getFreeVariables(new HashSet<>()), new HashSet<>());
    }

    public Set<Variable> getFreeVariables() {
        return getFreeVariables(new HashSet<>());
    }

    public abstract Set<Variable> getFreeVariables(Set<Variable> bounded);

    public abstract boolean bounds(Variable v, Set<Variable> free, Set<Variable> bounded);

    public abstract FunctionalExpression tryGetSubstitution(Variable v, Expression expression);

    private static class Parser {
        public static LogicalExpression parseExpression(LexemesStream l) {
            List<LogicalExpression> expressions = new ArrayList<>();
            do {
                expressions.add(parseDisjunction(l));
            } while (!l.isEmpty() && l.pop().equals("->"));
            while (expressions.size() != 1) {
                LogicalExpression e1 = expressions.remove(expressions.size() - 2);
                LogicalExpression e2 = expressions.remove(expressions.size() - 1);
                expressions.add(new Implication(e1, e2));
            }
            return expressions.get(0);
        }

        private static LogicalExpression parseDisjunction(LexemesStream l) {
            LogicalExpression expression = parseConjunction(l);
            while (!l.isEmpty() && l.peek().equals("|")) {
                l.pop();
                expression = new Or(expression, parseConjunction(l));
            }
            return expression;
        }

        private static LogicalExpression parseConjunction(LexemesStream l) {
            LogicalExpression expression = parseUnary(l);
            while (!l.isEmpty() && l.peek().equals("&")) {
                l.pop();
                expression = new And(expression, parseUnary(l));
            }
            return expression;
        }

        private static LogicalExpression parseUnary(LexemesStream l) {
            if (isName(l.peek(), true)) {
                return parsePredicate(l);
            } else if (l.peek().equals("!")) {
                l.pop();
                return new Not(parseUnary(l));
            } else if (l.peek().equals("?") || l.peek().equals("@")) {
                return parseQuantifier(l);
            } else if (l.peek().equals("(")) {
                LexemesStream.Node node = l.head.next;
                int count = 1;
                while (count > 0) {
                    if (node.data.equals("(")) {
                        count++;
                    } else if (node.data.equals(")")) {
                        count--;
                    }
                    node = node.next;
                }
                boolean equals = false;
                if (node != null) {
                    equals = node.data.equals("=") || node.data.equals("+") || node.data.equals("*") || node.data.equals("'");
                }
                if (!equals) {
                    l.pop();
                    return parseExpression(l);
                }
            }
            return parseEquals(l);
        }

        private static Predicate parsePredicate(LexemesStream l) {
            String name = l.pop();
            List<FunctionalExpression> args = new ArrayList<>();
            if (!l.isEmpty() && l.peek().equals("(")) {
                l.pop();
                do {
                    args.add(parseTerm(l));
                } while (l.pop().equals(","));
            }
            return new Predicate(name, args);
        }

        private static Quantifier parseQuantifier(LexemesStream l) {
            boolean e = l.pop().equals("?");
            Variable v = parseVariable(l);
            return e ? new ExistsQuantifier(v, parseUnary(l)) : new ForAllQuantifier(v, parseUnary(l));
        }

        private static Predicate parseEquals(LexemesStream l) {
            List<FunctionalExpression> args = new ArrayList<>();
            args.add(parseTerm(l));
            l.pop();
            args.add(parseTerm(l));
            return new Predicate("=", args);
        }

        private static FunctionalExpression parseTerm(LexemesStream l) {
            FunctionalExpression expression = parseAdditional(l);
            while (!l.isEmpty() && l.peek().equals("+")) {
                l.pop();
                List<FunctionalExpression> args = new ArrayList<>();
                args.add(expression);
                args.add(parseAdditional(l));
                expression = new Function("+", args);
            }
            return expression;
        }

        private static FunctionalExpression parseAdditional(LexemesStream l) {
            FunctionalExpression expression = parseInc(l);
            while (!l.isEmpty() && l.peek().equals("*")) {
                l.pop();
                List<FunctionalExpression> args = new ArrayList<>();
                args.add(expression);
                args.add(parseInc(l));
                expression = new Function("*", args);
            }
            return expression;
        }

        public static FunctionalExpression parseInc(LexemesStream l) {
            FunctionalExpression expression = parseMultiplied(l);
            while (!l.isEmpty() && l.peek().equals("'")) {
                List<FunctionalExpression> args = new ArrayList<>();
                args.add(expression);
                l.pop();
                expression = new Function("'", args);
            }
            return expression;
        }

        private static FunctionalExpression parseMultiplied(LexemesStream l) {
            if (isName(l.peek(), false)) {
                if (l.head.next != null && l.head.next.data.equals("(")) {
                    return parseFunction(l);
                }
                return parseVariable(l);
            } else if (l.peek().equals("0")) {
                return new Function(l.pop(), new ArrayList<>());
            }
            l.pop();
            FunctionalExpression expression = parseTerm(l);
            l.pop();
            return expression;
        }

        private static Function parseFunction(LexemesStream l) {
            String name = l.pop();
            l.pop();
            List<FunctionalExpression> args = new ArrayList<>();
            do {
                args.add(parseTerm(l));
            } while (!l.isEmpty() && l.pop().equals(","));
            return new Function(name, args);
        }

        private static Variable parseVariable(LexemesStream l) {
            return new Variable(l.pop());
        }

        private static boolean isName(String s, boolean predicate) {
            if ((s.charAt(0) >= 'A' && s.charAt(0) <= 'Z' && predicate) || (s.charAt(0) >= 'a' && s.charAt(0) <= 'z' && !predicate)) {
                for (int i = 1; i < s.length(); i++) {
                    if (s.charAt(i) < '0' || s.charAt(i) > '9') {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }

    private static class Lexer {
        private static LexemesStream getLexemes(String s) {
            List<String> lexemes = new ArrayList<>();
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (c == '-' && s.charAt(i + 1) == '>') {
                    lexemes.add("->");
                    i++;
                } else if (c == '|' || c == '&' || c == '!' || c == '(' || c == ')'
                        || c == '@' || c == '?' || c == ',' || c == '+' || c == '*'
                        || c == '\'' || c == '=' || c == '0') {
                    lexemes.add(s.substring(i, i + 1));
                } else {
                    String name = s.substring(i, ++i);
                    for (; i < s.length() && s.charAt(i) >= '0' && s.charAt(i) <= '9'; i++) {
                        name += s.charAt(i);
                    }
                    lexemes.add(name);
                    i--;
                }
            }
            return new LexemesStream(lexemes);
        }
    }

    private static class LexemesStream {
        private Node head;

        public LexemesStream(List<String> lexemes) {
            for (int i = lexemes.size() - 1; i >= 0; i--) {
                push(lexemes.get(i));
            }
        }

        public void push(String s) {
            head = new Node(head, s);
        }

        public String pop() {
            String s = head.data;
            head = head.next;
            return s;
        }

        public boolean isEmpty() {
            return head == null;
        }

        public String peek() {
            return head.data;
        }

        @Override
        public String toString() {
            Node n = head;
            String res = "";
            while (n != null) {
                res += "{" + n.data + "}";
                n = n.next;
            }
            return res;
        }

        private static class Node {
            private Node next;
            private String data;

            public Node(Node next, String data) {
                this.next = next;
                this.data = data;
            }
        }
    }
}
