package info.mermakov.projects.ifmo.mathlogic.helper;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.ExistsQuantifier;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.ForAllQuantifier;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.LogicalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.binary.Implication;

import java.io.*;
import java.util.*;


public class Solver {
    private static List<String> aiStrings = new ArrayList<>();
    private static List<Integer> aiIntegers = new ArrayList<>();
    private static List<String> iaStrings = new ArrayList<>();
    private static List<Integer> iaIntegers = new ArrayList<>();
    private static List<String> swapStrings = new ArrayList<>();
    private static List<Integer> swapIntegers = new ArrayList<>();

    public static void solve(boolean isFourth, String fileIn, String fileOut) throws IOException {
        init(new BufferedReader(new FileReader(new File("config/config1.in"))), iaStrings, iaIntegers);
        init(new BufferedReader(new FileReader(new File("config/config2.in"))), aiStrings, aiIntegers);
        init(new BufferedReader(new FileReader(new File("config/config3.in"))), swapStrings, swapIntegers);
        Scanner sc = new Scanner(new File(fileIn));
        BufferedWriter out = new BufferedWriter(new FileWriter(new File(fileOut)));
        if (isFourth) {

            String s = sc.nextLine();
            List<LogicalExpression> a = new ArrayList<>();
            LogicalExpression aim = null;
            Set<Expression> all = new HashSet<>();
            int last = 0;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == ',') {
                    a.add(Expression.parseExpression(s.substring(last, i)));
                    last = i + 1;
                } else if (s.charAt(i) == '|' && s.charAt(i + 1) == '-') {
                    a.add(Expression.parseExpression(s.substring(last, i)));
                    aim = Expression.parseExpression(s.substring(i + 2));
                    break;
                }
            }
            LogicalExpression ex = a.remove(a.size() - 1);
            Set<Variable> nf = ex.getFreeVariables();
            for (int i = 0; i < a.size(); i++) {
                out.write(a.get(i).toString() + (i == a.size() - 1 ? "" : ","));
            }
            out.write("|-");
            out.write(new Implication(ex, aim).toString() + "\n");
            int line = 0;
            while (sc.hasNext()) {
                String error = "";
                String l = sc.nextLine();
                LogicalExpression e = Expression.parseExpression(l);
                l = e.toString();
                boolean res = false;
                if (e.equals(ex)) {
                    out.write(l + "->(" + l + "->" + l + ")\n(" + l + "->(" + l + "->" + l + "))->(" + l + "->((" + l + "->" + l + ")->" + l + "))->(" + l + "->" + l + ")\n(" + l + "->((" + l + "->" + l + ")->" + l + "))->(" + l + "->" + l + ")\n(" + l + "->((" + l + "->" + l + ")->" + l + "))\n" + l + "->" + l + "\n");
                    res = true;
                } else if (a.contains(e)) {
                    out.write(e + "->(" + ex + "->" + e + ")\n" + e + "\n" + ex + "->" + e + "\n");
                    res = true;
                } else if (e.isAxiom()) {
                    out.write(l + "\n" + l + "->(" + ex + "->" + l + ")\n" + ex + "->" + l + "\n");
                    res = true;
                } else {
                    for (Expression expr : all) {
                        if (expr instanceof Implication && ((Implication) expr).right.equals(e)) {
                            if (all.contains(((Implication) expr).left)) {
                                Expression mps = ((Implication) expr).left;
                                out.write("(" + ex + "->" + mps + ")->((" + ex + "->(" + mps + "->" + l + "))->(" + ex + "->" + l + "))\n((" + ex + "->(" + mps + "->" + l + "))->(" + ex + "->" + l + "))\n" + ex + "->" + l + "\n");
                                res = true;
                                break;
                            }
                        }
                    }
                }
                if (!res && e instanceof Implication) {
                    if (((Implication) e).right instanceof ForAllQuantifier) {
                        if (!nf.contains(((ForAllQuantifier) ((Implication) e).right).variable)) {
                            if (all.contains(new Implication(((Implication) e).left, ((ForAllQuantifier) ((Implication) e).right).expression))) {
                                if (!((Implication) e).left.getFreeVariables().contains(((ForAllQuantifier) ((Implication) e).right).variable)) {
                                    first(out, ex.toString(), ((Implication) e).left.toString(), ((ForAllQuantifier) ((Implication) e).right).expression.toString(), ((ForAllQuantifier) ((Implication) e).right).variable.toString());
                                    res = true;
                                } else {
                                    error = "Variable " + ((ForAllQuantifier) ((Implication) e).right).variable + " is free in" + ((Implication) e).left;
                                }
                            }
                        } else {
                            error = "Using free variable from context";
                        }
                    }
                    if (!res && ((Implication) e).left instanceof ExistsQuantifier) {
                        if (!nf.contains(((ExistsQuantifier) ((Implication) e).left).variable)) {
                            if (all.contains(new Implication(((ExistsQuantifier) ((Implication) e).left).expression, (((Implication) e).right)))) {
                                if (!((Implication) e).right.getFreeVariables().contains(((ExistsQuantifier) ((Implication) e).left).variable)) {
                                    second(out, ex.toString(), ((ExistsQuantifier) ((Implication) e).left).expression.toString(), ((Implication) e).right.toString(), ((ExistsQuantifier) ((Implication) e).left).variable.toString());
                                    res = true;
                                } else {
                                    error = "Variable " + ((ExistsQuantifier) ((Implication) e).left).variable + "is free in " + ((Implication) e).right;
                                }
                            }
                        } else {
                            error = "Using free variable from context";
                        }
                    }
                }
                if (!res) {
                    out.close();
                    out = new BufferedWriter(new FileWriter(new File(fileOut)));
                    if (error.equals("")) {
                        error = LogicalExpression.error;
                    }
                    if (!error.equals("")) {
                        error = ", error: " + error;
                    }
                    out.write("Error: line: " + line + error + "\n");
                    out.close();
                    return;
                }
                all.add(e);
                line++;
            }
            out.close();
        } else {
            Set<LogicalExpression> expressions = new HashSet<>();
            int line = 0;
            while (sc.hasNext()) {
                String error = "";
                LogicalExpression e = Expression.parseExpression(sc.nextLine());
                boolean result = false;
                if (e.isAxiom()) {
                    result = true;
                } else if (e instanceof Implication) {
                    LogicalExpression left = ((Implication) e).left, right = ((Implication) e).right;
                    if (right instanceof ForAllQuantifier) {
                        Variable v = ((ForAllQuantifier) right).variable;
                        LogicalExpression expression = ((ForAllQuantifier) right).expression;
                        if (expressions.contains(new Implication(left, expression))) {
                            if (!left.getFreeVariables().contains(v)) {
                                result = true;
                            } else {
                                error = "Variable " + v + " is free in " + left;
                            }
                        }
                    }
                    if (!result && left instanceof ExistsQuantifier) {
                        Variable v = ((ExistsQuantifier) left).variable;
                        LogicalExpression expression = ((ExistsQuantifier) left).expression;
                        if (expressions.contains(new Implication(expression, right))) {
                            if (!right.getFreeVariables().contains(v)) {
                                result = true;
                            } else {
                                error = "Variable " + v + " is free in " + right;
                            }
                        }
                    }
                }
                if (!result) {
                    for (Expression expression : expressions) {
                        if (expression instanceof Implication) {
                            Expression left = ((Implication) expression).left, right = ((Implication) expression).right;
                            if (right.equals(e) && expressions.contains(left)) {
                                result = true;
                            }
                        }
                    }
                }
                if (!result) {
                    if (error.equals("")) {
                        error = LogicalExpression.error;
                    }
                    if (!error.equals("")) {
                        error = ", error: " + error;
                    }
                    System.out.println("Fail, line : " + line + error);
                    out.write("Fail, line : " + line + error);
                    out.close();
                    return;
                }
                expressions.add(e);
                line++;
            }
            out.write("Ok\n");
            System.out.println("Ok");
            out.close();
        }
    }

    private static void first(Writer out, String a, String b, String c, String name) throws IOException {
        out.write(get(iaStrings, iaIntegers, a, b, c));
        out.write(a + "&" + b + "->" + c + "\n" + a + "&" + b + "->" + "@" + name + "(" + c + ")\n");
        out.write(get(aiStrings, aiIntegers, a, b, "@" + name + "(" + c + ")"));
        out.write(a + "->" + b + "->" + "@" + name + "(" + c + ")\n");
    }

    private static void second(Writer out, String a, String b, String c, String name) throws IOException {
        out.write(get(swapStrings, swapIntegers, a, b, c));
        out.write(b + "->" + a + "->" + c + "\n?" + name + "(" + b + ")->" + a + "->" + c + "\n");
        out.write(get(swapStrings, swapIntegers, "?" + name + "(" + b + ")", a, c));
        out.write(a + "->?" + name + "(" + b + ")->" + c + "\n");
    }

    private static void init(BufferedReader sc, List<String> sl, List<Integer> il) throws IOException {
        while (sc.ready()) {
            String in = sc.readLine() + "\n";
            int last = 0;
            for (int i = 0; i < in.length(); i++) {
                if (in.charAt(i) >= 'A' && in.charAt(i) <= 'C') {
                    sl.add(in.substring(last, i));
                    il.add(in.charAt(i) == 'A' ? 0 : in.charAt(i) == 'B' ? 1 : 2);
                    last = i + 1;
                }
            }
            sl.add(in.substring(last, in.length()));
            il.add(4);
        }
    }

    private static String get(List<String> sl, List<Integer> il, String a, String b, String c) throws IOException {
        String res = "";
        for (int i = 0; i < sl.size(); i++) {
            res += sl.get(i);
            res += il.get(i) == 4 ? "" : il.get(i) == 0 ? a : il.get(i) == 1 ? b : c;
        }
        return res;
    }
}
