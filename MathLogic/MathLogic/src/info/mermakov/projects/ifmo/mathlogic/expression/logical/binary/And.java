package info.mermakov.projects.ifmo.mathlogic.expression.logical.binary;

import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.LogicalExpression;

import java.util.List;

public class And extends BinaryLogicalExpression {
    public And(LogicalExpression left, LogicalExpression right) {
        super(left, right);
    }

    @Override
    public And substitute(Variable v, FunctionalExpression expression) {
        return new And(left.substitute(v, expression), right.substitute(v, expression));
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof And && left.equals(((And) o).left) && right.equals(((And) o).right);
    }

    @Override
    public int hashCode() {
        return 2 * left.hashCode() + 3 * right.hashCode() + 5;
    }

    @Override
    public boolean isTemplateFor(LogicalExpression expression, List<LogicalExpression> state) {
        return expression instanceof And && left.isTemplateFor(((And) expression).left, state) && right.isTemplateFor(((And) expression).right, state);
    }

    @Override
    public String toString() {
        return "(" + left + "&" + right + ")";
    }
}
