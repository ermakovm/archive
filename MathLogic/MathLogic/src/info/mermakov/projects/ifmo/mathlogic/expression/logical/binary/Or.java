package info.mermakov.projects.ifmo.mathlogic.expression.logical.binary;

import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.LogicalExpression;

import java.util.List;

public class Or extends BinaryLogicalExpression {
    public Or(LogicalExpression left, LogicalExpression right) {
        super(left, right);
    }

    @Override
    public Or substitute(Variable v, FunctionalExpression expression) {
        return new Or(left.substitute(v, expression), right.substitute(v, expression));
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Or && left.equals(((Or) o).left) && right.equals(((Or) o).right);
    }

    @Override
    public int hashCode() {
        return 17 * left.hashCode() + 19 * right.hashCode() + 23;
    }

    @Override
    public boolean isTemplateFor(LogicalExpression expression, List<LogicalExpression> state) {
        return expression instanceof Or && left.isTemplateFor(((Or) expression).left, state) && right.isTemplateFor(((Or) expression).right, state);
    }

    @Override
    public String toString() {
        return "(" + left + "|" + right + ")";
    }
}
