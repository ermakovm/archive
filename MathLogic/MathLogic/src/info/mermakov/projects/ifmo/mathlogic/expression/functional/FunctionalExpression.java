package info.mermakov.projects.ifmo.mathlogic.expression.functional;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;

public abstract class FunctionalExpression extends Expression {

    @Override
    public abstract FunctionalExpression substitute(Variable v, FunctionalExpression expression);
}
