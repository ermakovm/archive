package info.mermakov.projects.ifmo.mathlogic.expression.logical.binary;

import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;
import info.mermakov.projects.ifmo.mathlogic.expression.logical.LogicalExpression;

import java.util.List;

public class Implication extends BinaryLogicalExpression {
    public Implication(LogicalExpression left, LogicalExpression right) {
        super(left, right);
    }

    @Override
    public Implication substitute(Variable v, FunctionalExpression expression) {
        return new Implication(left.substitute(v, expression), right.substitute(v, expression));
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Implication && left.equals(((Implication) o).left) && right.equals(((Implication) o).right);
    }

    @Override
    public int hashCode() {
        return 7 * left.hashCode() + 11 * right.hashCode() + 13;
    }

    @Override
    public boolean isTemplateFor(LogicalExpression expression, List<LogicalExpression> state) {
        return expression instanceof Implication && left.isTemplateFor(((Implication) expression).left, state) && right.isTemplateFor(((Implication) expression).right, state);
    }

    @Override
    public String toString() {
        return "(" + left + "->" + right + ")";
    }
}
