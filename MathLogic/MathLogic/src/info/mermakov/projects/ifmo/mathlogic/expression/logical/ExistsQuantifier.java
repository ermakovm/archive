package info.mermakov.projects.ifmo.mathlogic.expression.logical;

import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;

public class ExistsQuantifier extends Quantifier {
    public ExistsQuantifier(Variable variable, LogicalExpression expression) {
        super(variable, expression);
    }

    @Override
    public ExistsQuantifier substitute(Variable v, FunctionalExpression fe) {
        return v.equals(variable) ? this : new ExistsQuantifier(variable, expression.substitute(v, fe));
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ExistsQuantifier && variable.equals(((ExistsQuantifier) o).variable) && expression.equals(((ExistsQuantifier) o).expression);
    }

    @Override
    public int hashCode() {
        return variable.hashCode() - 2 * expression.hashCode() + 3;
    }

    @Override
    public String toString() {
        return "(?" + variable + expression + ")";
    }
}
