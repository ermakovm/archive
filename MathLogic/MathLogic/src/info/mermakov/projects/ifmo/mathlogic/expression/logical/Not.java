package info.mermakov.projects.ifmo.mathlogic.expression.logical;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;

import java.util.List;
import java.util.Set;

public class Not extends LogicalExpression {
    private LogicalExpression expression;

    public Not(LogicalExpression expression) {
        this.expression = expression;
    }

    @Override
    public Not substitute(Variable v, FunctionalExpression fe) {
        return new Not(expression.substitute(v, fe));
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Not && expression.equals(((Not) o).expression);
    }

    @Override
    public int hashCode() {
        return 31 - 3 * expression.hashCode();
    }

    @Override
    public Set<Variable> getFreeVariables(Set<Variable> bounded) {
        return expression.getFreeVariables(bounded);
    }

    @Override
    public boolean bounds(Variable v, Set<Variable> free, Set<Variable> bounded) {
        return expression.bounds(v, free, bounded);
    }

    @Override
    public boolean isTemplateFor(LogicalExpression expr, List<LogicalExpression> state) {
        return expr instanceof Not && expression.isTemplateFor(((Not) expr).expression, state);
    }

    @Override
    public FunctionalExpression tryGetSubstitution(Variable v, Expression expr) {
        if (expr instanceof Not) {
            return expression.tryGetSubstitution(v, ((Not) expr).expression);
        }
        return null;
    }

    @Override
    public String toString() {
        return "(!" + expression + ")";
    }
}
