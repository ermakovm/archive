package info.mermakov.projects.ifmo.mathlogic.expression.logical;

import info.mermakov.projects.ifmo.mathlogic.expression.Expression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.FunctionalExpression;
import info.mermakov.projects.ifmo.mathlogic.expression.functional.Variable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Template extends LogicalExpression {
    private int number;

    public Template(int number) {
        this.number = number;
    }

    @Override
    public Template substitute(Variable v, FunctionalExpression expression) {
        return this;
    }

    @Override
    public boolean bounds(Variable v, Set<Variable> free, Set<Variable> bounded) {
        return false;
    }

    @Override
    public Set<Variable> getFreeVariables(Set<Variable> bounded) {
        return new HashSet<>();
    }

    @Override
    public boolean isTemplateFor(LogicalExpression expression, List<LogicalExpression> state) {
        while (state.size() <= number) {
            state.add(null);
        }
        if (state.get(number) == null) {
            state.set(number, expression);
            return true;
        }
        return state.get(number).equals(expression);
    }

    @Override
    public FunctionalExpression tryGetSubstitution(Variable v, Expression expression) {
        return null;
    }
}