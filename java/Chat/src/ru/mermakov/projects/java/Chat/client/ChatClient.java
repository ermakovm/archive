package ru.mermakov.projects.java.Chat.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author Ermakov Mikhail
 * @version 1.0
 *
 * client for chat
 *
 * @see java.lang.Runnable
 * @see java.net.Socket
 * @see java.lang.Thread
 * @see java.io.DataInputStream
 * @see java.io.DataOutputStream
 * @see ru.mermakov.projects.java.Chat.client.ChatClientThread
 */
public class ChatClient implements Runnable {
    private Socket socket = null;
    private Thread thread = null;
    private DataInputStream console = null;
    private DataOutputStream streamOut = null;
    private ChatClientThread client = null;
    public static String username=null;

    /**
     * This constructor initializes socket and start client
     *
     * @param serverName server address
     * @param serverPort server port
     *
     * @see java.io.IOException
     * @see java.net.UnknownHostException
     */
    public ChatClient(String serverName, int serverPort) {
        System.out.println("Establishing connection. Please wait ...");
        try {
            socket = new Socket(serverName, serverPort);
            System.out.println("Connected: " + socket);
            start();
        } catch (UnknownHostException uhe) {
            System.out.println("Host unknown: " + uhe.getMessage());
        } catch (IOException ioe) {
            System.out.println("Unexpected exception: " + ioe.getMessage());
        }
    }

    /**
     * write to out from console
     * @see java.io.IOException
     * @see java.lang.Thread
     */
    public void run() {
        while (thread != null) {
            try {
                streamOut.writeUTF(console.readLine());
                streamOut.flush();
            } catch (IOException ioe) {
                System.out.println("Sending error: " + ioe.getMessage());
                stop();
            }
        }
    }

    /**
     * check message for exit command and write message to console
     *
     * @param msg message
     */
    public void handle(String msg) {
        if (msg.contains(".bye")) {
            System.out.println("Good bye. Press RETURN to exit ...");
            stop();
        } else
            System.out.println(msg);
    }

    /**
     * start new client tread and open DataInputStream and DataOutputStream
     *
     * @throws IOException
     * @see java.io.IOException
     * @see java.io.DataInputStream
     * @see java.io.DataOutputStream
     * @see java.lang.Thread
     * @see ru.mermakov.projects.java.Chat.client.ChatClientThread
     */
    public void start() throws IOException {
        console = new DataInputStream(System.in);
        streamOut = new DataOutputStream(socket.getOutputStream());
        if (thread == null) {
            client = new ChatClientThread(this, socket);
            thread = new Thread(this);
            thread.start();
        }
    }

    /**
     * stop this client
     * @see java.io.IOException
     */
    public void stop() {
        if (thread != null) {
            thread.stop();
            thread = null;
        }
        try {
            if (console != null) console.close();
            if (streamOut != null) streamOut.close();
            if (socket != null) socket.close();
        } catch (IOException ioe) {
            System.out.println("Error closing ...");
        }
        client.close();
        client.stop();
    }

    /**
     * start client
     * @param args args should constains 3 argument: host,port,username
     */
    public static void main(String args[]) {
        ChatClient client = null;
        if (args.length != 3) {
            System.out.println("Usage: java ChatClient host port username");
        }
        else {
            username=args[2];
            client = new ChatClient(args[0], Integer.parseInt(args[1]));
        }
    }
}
