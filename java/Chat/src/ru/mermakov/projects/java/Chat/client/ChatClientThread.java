package ru.mermakov.projects.java.Chat.client;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * @author Ermakov Mikhail
 * @version 1.0
 *
 * @see java.lang.Thread
 * @see ru.mermakov.projects.java.Chat.client.ChatClient
 * @see java.io.DataInputStream
 */
public class ChatClientThread extends Thread {
    private Socket socket = null;
    private ChatClient client = null;
    private DataInputStream streamIn = null;


    /**
     *  This constructor initializes <code>_client</code> and <code>_socket</code> then start thread
     * @param _client client
     * @param _socket socket
     *
     * @see ru.mermakov.projects.java.Chat.client.ChatClient
     * @see java.net.Socket
     */
    public ChatClientThread(ChatClient _client, Socket _socket) {
        client = _client;
        socket = _socket;
        open();
        start();
    }

    /**
     * open DataInputStream
     *
     * @see java.io.DataInputStream
     * @see java.io.IOException
     */
    public void open() {
        try {
            streamIn = new DataInputStream(socket.getInputStream());
        } catch (IOException ioe) {
            System.out.println("Error getting input stream: " + ioe);
            client.stop();
        }
    }

    /**
     * close DataInputStream
     *
     * @see java.io.DataInputStream
     * @see java.io.IOException
     */
    public void close() {
        try {
            if (streamIn != null) streamIn.close();
        } catch (IOException ioe) {
            System.out.println("Error closing input stream: " + ioe);
        }
    }


    /**
     * Waiting messages from server
     *
     * @see ru.mermakov.projects.java.Chat.client.ChatClient
     * @see java.io.IOException
     */
    public void run() {
        while (true) {
            try {
                client.handle(ChatClient.username+"> "+streamIn.readUTF());
            } catch (IOException ioe) {
                System.out.println("Listening error: " + ioe.getMessage());
                client.stop();
            }
        }
    }
}
