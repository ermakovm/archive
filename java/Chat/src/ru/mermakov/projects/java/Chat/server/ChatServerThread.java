package ru.mermakov.projects.java.Chat.server;

import java.io.*;
import java.net.Socket;

/**
 * @author Ermakov Mikhail
 * @version 1.0
 *
 * Thread for connected client
 *
 * @see java.lang.Thread
 */
public class ChatServerThread extends Thread {
    private ChatServer server = null;
    private Socket socket = null;
    private int ID = -1;
    private DataInputStream streamIn = null;
    private DataOutputStream streamOut = null;

    /**
     * This constructor initializes the fields in this class
     *
     * @param _server Chat server
     * @param _socket socket
     * @see ru.mermakov.projects.java.Chat.server.ChatServer
     * @see java.net.Socket
     */
    public ChatServerThread(ChatServer _server, Socket _socket) {
        super();
        server = _server;
        socket = _socket;
        ID = socket.getPort();
    }

    /**
     * Send message for current client,If an error occurs, the client is disconnected from the server
     * @param msg
     */
    public void send(String msg) {
        try {
            streamOut.writeUTF(msg);
            streamOut.flush();
        } catch (IOException ioe) {
            System.out.println(ID + " ERROR sending: " + ioe.getMessage());
            server.remove(ID);
            stop();
        }
    }

    /**
     * this method returns the id of the current thread
     *
     * @return ID
     */
    public int getID() {
        return ID;
    }

    /**
     * Start new thread for client
     */
    public void run() {
        System.out.println("Server Thread " + ID + " running.");
        while (true) {
            try {
                server.handle(ID, streamIn.readUTF());
            } catch (IOException ioe) {
                System.out.println(ID + " ERROR reading: " + ioe.getMessage());
                server.remove(ID);
                stop();
            }
        }
    }

    /**
     * Open DataInputStream and DataOutputStream
     *
     * @throws IOException
     * @see java.io.DataInputStream
     * @see java.io.DataOutputStream
     * @see java.io.BufferedInputStream
     * @see java.io.BufferedOutputStream
     */
    public void open() throws IOException {
        streamIn = new DataInputStream(new
                BufferedInputStream(socket.getInputStream()));
        streamOut = new DataOutputStream(new
                BufferedOutputStream(socket.getOutputStream()));
    }

    /**
     * Close DataInputStream and DataOutputStream and Socket
     *
     * @throws IOException
     * @see java.io.DataInputStream
     * @see java.io.DataOutputStream
     * @see java.net.Socket
     */
    public void close() throws IOException {
        if (socket != null) socket.close();
        if (streamIn != null) streamIn.close();
        if (streamOut != null) streamOut.close();
    }
}
