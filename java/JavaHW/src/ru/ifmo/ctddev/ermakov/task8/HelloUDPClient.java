package ru.ifmo.ctddev.ermakov.task8;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Ermakov Mikhail
 *      Client for server
 * @see java.lang.Thread
 * @see java.net.InetAddress
 */

public class HelloUDPClient {

    final Thread[] threads;
    final InetAddress address;
    final int port;
    final String pref;

    static final int COL_THREADS = 10;
    static final int MAX_MESSAGE_COUNT = 3;

    /**
     * Constructor of this class
     *
     * @param host adress of server
     * @param port port of server
     * @param prefix
     * @throws UnknownHostException
     */
    public HelloUDPClient(String host, int port, String prefix)
            throws UnknownHostException {
        this.address = Inet4Address.getByName(host);
        this.port = port;
        this.pref = prefix;
        this.threads = new Thread[COL_THREADS];
    }

    /**
     * start threads (count = COL_THREADS)
     */
    public void start() {
        for (int i = 0; i < COL_THREADS; i++) {
            threads[i] = new Thread(new Sender(address, port, pref,
                    Integer.toString(i)));
        }
        for (Thread thread : threads) {
            thread.start();
        }
    }

    public static void main(String[] args) {
        if (args.length != 3) {
            System.err
                    .print("Incorrect parameters. Use: java HelloUDPClient host port prefix.");
        } else {
            String hname = args[0];
            int port = Integer.valueOf(args[1]);
            String prefix = args[2];
            try {
                (new HelloUDPClient(hname, port, prefix)).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}