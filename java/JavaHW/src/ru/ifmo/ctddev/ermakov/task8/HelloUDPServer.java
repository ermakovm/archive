package ru.ifmo.ctddev.ermakov.task8;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Ermakov Mikhail
 *
 *      UDP Server recieve pacets from client
 *
 * @see ru.ifmo.ctddev.ermakov.task8.HelloUDPClient
 * @see java.util.concurrent.ExecutorService
 * @see java.net.DatagramSocket
 *
 */

public class HelloUDPServer {
    private static final int THREAD_COUNT = 10;
    private static final int BUFFER_SIZE = 512;
    private ExecutorService exe;
    private DatagramSocket socket;
    private final int port;


    /**
     * Constructor of this class
     *
     * @param port server listen on this port
     * @throws Exception
     */
    public HelloUDPServer(int port) throws Exception {
        this.port = port;
        if (!(0 <= port && port < (1 << 16))) {
            throw new Exception("Invalid port number");
        }
    }


    /**
     * start new execuor service
     */
    public void start() {
        exe = Executors.newFixedThreadPool(THREAD_COUNT);
        try {
            socket = new DatagramSocket(port);
            while (true) {
                try {
                    DatagramPacket messagePacket = new DatagramPacket(new byte[BUFFER_SIZE], 0,
                            BUFFER_SIZE);
                    socket.receive(messagePacket);
                    exe.execute(new Replier(messagePacket));
                } catch (IOException e) {
                    System.err.println("Problem with receiving packet: "
                            + e.getMessage());
                }
            }
        } catch (SocketException e) {
            System.err.println("Socket problem. " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err
                    .print("Incorrect parameters. Use: java HelloUDPServer port");
        } else {
            int port = Integer.valueOf(args[0]);
            try {
                new HelloUDPServer(port).start();
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }
}