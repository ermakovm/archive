package ru.ifmo.ctddev.ermakov.task8;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @author Ermakov Mikhail
 *
 * Send packets to server
 * @see java.net.InetAddress
 */
public class Sender implements Runnable {
    String senderId;
    InetAddress address;
    int port;
    String pref;

    static final int BUFFER_SIZE = 512;
    static final int COL_THREADS = 10;
    static final int MAX_MESSAGE_COUNT = 3;


    /**
     * constructor of this class
     *
     * @param a address of comp
     * @param p
     * @param pre prefix
     * @param senderId sender id
     */
    public Sender(InetAddress a, int p, String pre, String senderId) {
        this.senderId = senderId;
        pref = pre;
        port = p;
        address = a;
    }

    /**
     * start sender
     *
     * @see java.net.DatagramSocket
     * @see java.net.DatagramPacket
     */
    public void run() {
        try {
            DatagramSocket socket = new DatagramSocket();
            for (int i = 0; i < MAX_MESSAGE_COUNT; i++) {
                String message = pref + "[" + senderId + "][" + i + "]";

                ByteArrayOutputStream out = new ByteArrayOutputStream(
                        BUFFER_SIZE);
                (new DataOutputStream(out)).writeUTF(message);

                byte[] bufIn = new byte[BUFFER_SIZE];
                byte[] bufOut = out.toByteArray();

                DatagramPacket messagePacket = new DatagramPacket(bufOut, 0,
                        bufOut.length, address, port);
                socket.send(messagePacket);
                System.out.println("Message \"" + message
                        + "\" was sent to the server.");

                ByteArrayInputStream inputStream = new ByteArrayInputStream(
                        bufIn, 0, BUFFER_SIZE);
                DatagramPacket responsePacket = new DatagramPacket(bufIn, 0,
                        BUFFER_SIZE);
                socket.receive(responsePacket);
                System.out.println("Response \"" + (new DataInputStream(inputStream)).readUTF()
                        + "\" was received.");
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
