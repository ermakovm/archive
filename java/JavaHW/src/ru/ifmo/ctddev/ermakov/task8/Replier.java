package ru.ifmo.ctddev.ermakov.task8;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * @author Ermakov Mikhail
 *
 * reply from server
 *
 * @see java.net.DatagramPacket
 */


public class Replier implements Runnable {
    static final int BUFFER_SIZE = 512;
    private DatagramPacket packet;

    /**
     * constructor of this file
     * @param messagePacket
     */
    public Replier(DatagramPacket messagePacket) {
        this.packet = messagePacket;
    }

    /**
     * start replyer
     */
    public void run() {
        DatagramSocket socket;
        try {
            socket = new DatagramSocket();

            DataInputStream input = new DataInputStream(new ByteArrayInputStream(
                    packet.getData()));

            String message = input.readUTF();
            String resp = "Hello, " + message;

            System.out.println("Message \"" + message + "\" was received.");

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(
                    BUFFER_SIZE);
            (new DataOutputStream(outputStream)).writeUTF(resp);
            byte[] buffer = outputStream.toByteArray();
            DatagramPacket responsePacket = new DatagramPacket(buffer, 0,
                    buffer.length, packet.getAddress(),
                    packet.getPort());
            socket.send(responsePacket);
            System.out.println("Response \"" + resp
                    + "\" was sent to the client.");
        } catch (SocketException e) {
            System.err.println("Socket didn't create! "
                    + e.getMessage());
        } catch (IOException e) {
            System.err
                    .println("Packet didn't sent! " + e.getMessage());
        }
    }

}
