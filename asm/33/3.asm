;��������� �������� �������, ����������� �������������� ����� � ������� double precision 
;� ��������� ���������� ������.

;�������� �������:
;void dbl2str(const double *in, char *out_buf);

;� ���������� ���������� ������� � ������ ������ ���� �������� ��������� 
;0-��������������� ������������� ����� � ����:
;+0.12345e5
;�� ����: ���� �����, ����� "0.", 1+ ������ ��������, "e", �������� ���������� (0 ��� ����).
;������ ����� �������������� ����������������� � ��-�����, 
;������� ��������� � ����: +Inf, -Inf, NaN.

;���������� ������ ������ ������������� ���������� ������������: 
;�������� �������������� ������ ���� �������� ����� ��� ���������� � ���������� �������.
;� ����� ���������� ����������� ����� ��������. 
;� ������ ���������� ����������� ��������� - ������� ��������� � ������ ���������.
;������ ���� � ���� "+" � ���������� �������� ����� �� �������.

;��������� ������: fastcall64. �� ��������� ��������� ������ ��������, ���� �� �� ���������.
;������� ������ ���� ����������� � ����� .asm ����� � �� ������������ �����-���� ������� �������, 
;.asm ���� ������ ���� �������� � ������.
;������������ FPU ����������� (�� ��������������� - �� ��� ������ �� �����).
;� ����� ������ ���� �������� �� ������ 32 ����.

 

Westmere
global  dbl2str

section .data
	exp		dq		0
	sexp	dq		0
	man		dq		0
	ceil	times	32 dq 0
	fract	times	32 dq 0
	counter	dq 		0
	flag	dq		0
	sign	dq		8000000000000000h
	masks	dq		10000000000000h
	maskm	dq		10000000000000h-1
section .text
dbl2str:	
	pushaq
	mov	rsi,rdx		;RSI <= ����� ������
	mov rbx,[rcx]		;RBX <= ������������ �����
	

	;�������� �����
	mov al,'+'
	test rbx,[sign]	
	je	@plus
	xor rbx,[sign]	
	mov al,'-'
@plus:
	mov byte [rsi],al
	inc rsi

	cmp rbx,0
	jne @not
		mov byte [rsi],'0'
		inc rsi
		mov byte [rsi],'e'
		inc rsi
		mov byte [rsi],'0'
		inc rsi
		jmp @exit
	@not:

	;Exponent	Fraction	IEEE value
	;�AX	0XXXXXX	QNAN
	;�AX	1XXXXXX	SNAN
	;�AX	0	INF���� �������

	;���������� �������(����������) � �������� ������������� ������������� �����
	shr rbx,52
	sub	rbx,1023
	mov	[exp],rbx

	;�������� ��������
	mov	rbx,[rcx]
	and 	rbx,[maskm]
	xor	rbx,[masks]
	mov	[man],rbx

	;Exponent	Fraction	IEEE value
	;�AX	0XXXXXX	QNAN
	;�AX	1XXXXXX	SNAN
	;�AX	0	INF���� �������

	cmp	[exp],2047
	jne	@notNAN
	cmp	[man],0
	jne	@notINF
	mov byte [rsi],'I'
	mov byte [rsi+1],'n'
	mov byte [rsi+2],'f'
	add rsi,3
	jmp @exit	
@notINF:
	mov eax,[man]
	and eax,7FFFFFFFFFFFFh
	cmp eax,1023
	jne @notNAN
	dec	rsi
	mov byte [rsi],'N'
	mov byte [rsi+1],'a'
	mov byte [rsi+2],'N'
	add rsi,3
	jmp @exit	
@notNAN:


	mov byte [rsi],'0'
	inc rsi
	mov byte [rsi],','
	inc rsi
	
	;�������� ����� � ������� ����� ��������
	mov rax,[exp]
	mov qword [exp],0
	test rax,800h
	jne	@notceil	;������� ���� ��� ����� ����� ��� ������
	cmp	rax,52
	ja	@notfract	;������� ���� ��� ������� ��� ������
	
	mov rcx,52
	sub	rcx,rax
	mov rdx,[man]
	shr	rdx,cl
	mov [ceil],rdx
	mov	rdi,ceil
	push rax
	call OutputCeil
	pop	rax
	add	rax,12
	mov rcx,rax
	mov rdx,[man]
	shl	rdx,cl
	mov [fract+248],rdx
	jmp @fract
	
@notfract:	
	;����������� ������� ������ ��� �����
	xor	rdx,rdx
	sub	rax,52
	mov	rbx,64
	div	rbx
	mov	cl,dl
	mov rdx,[man]
	shld ceil[rax*8+8],rdx,cl
	shl	rdx,cl
	mov ceil[rax*8],rdx
	mov	rdi,ceil
	call OutputCeil
	jmp @extoutput
	
@notceil:	
	neg	rax
	dec	rax
	inc qword [sexp]
	mov rbx,64
	xor rdx,rdx
	div rbx
	mov rdi,32
	sub	rdi,rax
	mov rcx,64
	sub	rcx,rdx
	mov rbx,[man]
	shl	rbx,11
	mov rax,rbx
	cmp rcx,64
	je @notshl
		mov rax,0
		shld rax,rbx,cl
		mov cl,dl
		shl	rbx,cl
	@notshl:
	mov fract[rdi*8-8],rax
	cmp rcx,64
	jne @notzero
		mov rbx,0
	@notzero:
	mov fract[rdi*8-16],rbx
	mov qword [exp],1
@fract:	
	mov	rdi,fract
	call OutputFract

@extoutput:
	mov byte [rsi],'e'
	inc rsi
	cmp qword [sexp],1
	jne @notminus
		mov byte [rsi],'-'
		inc rsi
	@notminus:
	mov rax,[exp]
	mov [ceil],rax
	mov	rdi,ceil
	mov qword [exp],0
	call OutputCeil
@exit:
	mov byte [rsi],0
	popaq
	ret
end_dbl2str
		
OutputCeil:
	mov rbx,10
	@divide:
		xor	rdx,rdx
		mov	r8,32*8
		mov rcx,32
		@nextdiv:
			mov	rax,[r8+rdi-8]
			cmp rax,0
			jne	@next
				dec rcx
				cmp rcx,0
				je	@donrdiv
			@next:
			div	rbx
			mov	[r8+rdi-8],rax
			sub	r8,8
		jnz @nextdiv
		push rdx
		inc dword [exp]
	jmp @divide
@donrdiv:

	mov rcx,15
	cmp [exp],rcx
	jae	output
	mov rcx,[exp]
	output:
		pop	rdx
		add	dl,'0'
		mov byte [rsi],dl
		inc rsi
	loop output
	
	mov rcx,[exp]
	sub rcx,15
	js	@notpop
	@pop:
		pop rdx
	loop @pop
@notpop:
	ret
end_OutputCeil

OutputFract:
	mov rbx,10
	mov rax,[exp]
	mov [counter],rax
	mov qword [flag],0
	@mul:
		xor	rdx,rdx
		mov	r8,0
		mov rcx,0
		@nextmul:
			mov	rax,[r8+rdi]
			cmp rax,0
			jne	@nextm
				inc rcx
				cmp rcx,32
				je	@donemul
			@nextm:
			mul rbx
			mov	[r8+rdi],rax
			add	r8,8
			cmp r8,32*8
		jne @nextmul
		cmp qword [sexp],1
		jne	@notexps
		
		cmp	dl,0
		jne	@notexps
		cmp qword [flag],1
		je	@notexps
			inc dword [exp]
			jmp @zero
			
			
		@notexps:
		mov qword [flag],1
		inc qword [counter]
		cmp qword [counter],15
		ja	@donefract
			add	dl,'0'
			mov byte [rsi],dl
			inc rsi
		@donefract:	
	@zero:
	jmp @mul
@donemul:
	ret
end_OutputFract
