;��������� �������� �������, ����������� �������������� ����� �� ����������������� ������ 
;� ���������� �����������������.
;
;�������� �������:
;void print(char *out_buf, const char *format, const char *hex_number);
;
;������� ����������������� ����� 128-������, �� ������ � �������� �������������� ����. 
;����� ������ ����� ���� ������ '-', ������� ������� ���������������� ������ ��� 
;"������������� ��� ���� �������� ����� � �������� 1", 
;��� ������� ��� ����� ������������� �����. ����� ����� ���� ������ ��������.
;
;������ ������� ������� ������� ������� sprintf,�� ���������� ����������� ������ 
;��������� ������������: ����� '-', '+', ' ', '0' � ���� ������ (width).
;
;� �������� ����� ������ ���� �������� ����-��������������� ������ ���������� 
;��� ��������� ������ ��� ��� �����-���� ������ ��������.
;
;��������� ������: cdecl. �������� ������� � .asm �����: _print. 
;�� ��������� ��������� ������ ��������, ���� �� �� ���������.
;������� ������ ���� ����������� � ����� .asm ����� � �� ������������ 
;�����-���� ������� �������, ���� ������ .asm ���� ������ ���� �������� � ������.
;
;������ ������� ������: "-5", "10". ���������: "16   ".
;��� �������� �� ��������� ������ ������ ������������ ��������� ��� �� C:
;        int x;
;        sscanf("<hex_number>", "%x", &x);
;        printf("%<format>i", x);

Westmere
global  _print
section .const
	len		equ	16
section .data
	sign				db	0	;false
	dHex	times len/4	dd	0	;
	dDec				dd	0	;
	fdef	db 0
	fplus	db 0
	fprob	db 0
	fzero	db 0
	fwidth	db 0
section .text
_print:	
	push ebp
	push esp
	pop	ebp
	pushad
	mov esi,[ebp+16]	;hex_number	
	mov	edi,dHex
	xor	ecx,ecx
	xor	eax,eax
	cld
	lodsb
	cmp al,'-'
	jne	@next
	inc	byte [sign]
	lodsb
@next:
	inc	ecx
	cmp	ecx,len*2
	je	@ext
	cmp	al,0
	je	@ext
	cmp	al,'0'
	jb	@ext
	cmp	al,'9'
	ja	@next2
	sub	al,48
	jmp	@done
@next2:
	cmp	al,'a'
	jb	@next3
	cmp	al,'f'
	ja	@next3
	sub	al,87
	jmp	@done
@next3:
	cmp	al,'A'
	jb	@ext
	cmp	al,'F'
	ja	@ext
	sub	al,55
@done:
	mov	ebx,[edi+8]
	and	ebx,0f0000000h
	shr	ebx,28
	shl dword [edi+12],4
	add	dword [edi+12],ebx

	mov	ebx,[edi+4]
	and	ebx,0f0000000h
	shr	ebx,28
	shl dword [edi+8],4
	add	dword [edi+8],ebx

	mov	ebx,[edi]
	and	ebx,0f0000000h
	shr	ebx,28
	shl dword [edi+4],4
	add	dword [edi+4],ebx

	shl dword [edi],4
	add	dword [edi],eax
	lodsb
	jmp	@next
@ext:
;	cmp byte [sign],1
;	jne	@notminus

;	mov	ecx,len/4
;@minus:
;	not	dword [edi+ecx*4-4]
;	loop @minus

;	mov	ecx,3
;	inc dword [edi]
;@adc:
;	adc	dword [edi+4],0
;	add edi,4
;	loop @adc

@dec:
	mov ebx,10	
	xor edx,edx
	mov	ecx,len/4
	mov	esi,dHex+12
	std
@divide:
	lodsd 
	div	ebx
	mov	dword [esi+4],eax
	loop	@divide	
	inc dword [dDec]
	add		dx,'0'
	push	dx

	mov	ecx,len/4
@cmp:
	cmp dword [dHex+ecx*4-4],0
	jnz	@dec
	loop	@cmp

	cld
	mov esi,[ebp+12]	;format
@format:
	lodsb
	cmp	al,0
	je	@extf
	cmp	al,'-'
	jne	@notdef
	mov	byte [fdef],1
	jmp	@format
@notdef:
	cmp	al,'+'
	jne	@notplus
	mov	byte [fplus],1
	jmp	@format
@notplus:
	cmp	al,' '
	jne	@notprob
	mov	byte [fprob],1
	jmp	@format
@notprob:
	cmp	al,'0'
	jne	@notzero
	mov	byte [fzero],1
	jmp	@format
@notzero:
	cmp	al,'1'
	jb	@notwidth
	sub al,'0'
	add	byte [fwidth],al
	lodsb
	cmp	al,0
	je	@extf
	jmp	@notzero
@notwidth:
	jmp	@format

@extf:
	mov ecx,[dDec]
	mov esi,[ebp+8]
	cmp byte [sign],1
	je	@minus
	cmp byte [fplus],1
	jne	@sig
	cmp byte [sign],0
	je	@plus
@minus:
	mov	byte [esi],'-'
	jmp @sig
@plus:
	mov	byte [esi],'+'
@sig:
	cmp byte [fplus],1
	je	@notp
	cmp byte [sign],1
	je	@notp
	cmp byte [fprob],1
	jne	@out
	mov	byte [esi],' '
@notp:
	inc esi
	inc byte [dDec]
@out:
	pop dx
	mov byte [esi],dl
	inc esi
	loop @out
	
	cmp byte [fwidth],0
	je	@exit
	xor ecx,ecx
	mov	cl,[fwidth]
	cmp	ecx,[dDec]
	jbe	@exit
	sub	ecx,[dDec]
	cmp byte [fdef],1
	je	@defis

	cmp byte [fzero],1
	jne	@exit	
	mov byte [sign],0
	dec	esi
	mov	edi,esi
	add edi,ecx
	mov ebx,ecx
	mov ecx,[dDec]
	mov eax,[ebp+8]
	cmp byte [eax],'-'
	ja	@nexts
	mov byte [sign],1
@nexts:
	std
	rep movsb
	mov edi,[ebp+8]
	mov ecx,ebx
	cmp byte [sign],1
	jne	@notsig
	inc edi
@notsig:
	mov byte [edi],'0'
	inc edi
	loop @notsig
	cld
	mov cl,byte[fwidth]
	mov esi,[ebp+8]
	add esi,ecx
	jmp @exit
@defis:
	mov byte [esi],' '
	inc esi
	loop @defis
@exit:
	mov byte [esi],0
	popad
	pop ebp
    ret 
end__print
		

