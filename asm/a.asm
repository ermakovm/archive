	
	section .text 
global _mult  ; argument is not bigger than 16
_mult:
	push edi
	push esi
	mov edi, [esp + 4 + 8]
	mov ecx, [esp + 8 + 8]
	mov esi, [size]

	mult_run:
		mov al, [edi + esi]
		mul cl
		mov [edi + esi], al
		dec esi
	jnz mult_run
	pop esi
	
	push edi
	call _normalize
	add esp, 4
	
	pop edi
ret

global _normalize
_normalize:
	push edi
	push esi
	mov edi, 10
	mov esi, [esp + 4 + 8]
	mov ecx, [size]                                     ;
	dec ecx
	normalize_run:
		xor eax, eax
		xor edx, edx
		mov al, [esi + ecx]
		div edi
		mov [esi + ecx], dl
		add [esi + ecx - 1], al
		dec ecx
	jnz normalize_run
	pop esi
	pop edi
ret 
	
global _copy_string
_copy_string:
	push edi
	mov edi, [esp + 4 + 4]
	mov edx, [esp + 8 + 4]
	mov ecx, [size]
	copy_string_run:
		mov eax, [edi + ecx - 4]
		mov [edx + ecx - 4], eax
		sub ecx, 4
	jnz copy_string_run
	pop edi
ret
	
global _copy_small_string ; can copy in the same array (only in head part)
_copy_small_string:
	push edi
	push ebx
	mov edi, [esp + 4 + 8]
	mov edx, [esp + 8 + 8]
	mov ebx, [esp + 12 + 8]
	xor ecx, ecx
	copy_small_string_run:
		mov al, [edi + ecx]
		mov [edx + ecx], al
		inc ecx
		cmp ecx, ebx
	jnz copy_small_string_run
	pop ebx
	pop edi
ret

global _long_add
_long_add:
	push edi
	mov edi, [esp + 4 + 4]
	mov edx, [esp + 8 + 4]
	mov ecx, [size]
	long_add_run:
		mov eax, [edi + ecx - 4]
		add [edx + ecx - 4], eax
		sub ecx, 4
	jnz long_add_run
	pop edi

	push edx
	call _normalize
	add esp, 4
ret

global _fill  ;fill(array)
_fill:
	mov eax, [esp + 4]
	mov ecx, [size]						
	xor dl, dl
	fill_run:
		mov [eax + ecx - 1], dl
		dec ecx
	jnz fill_run
ret

global _fill_with  ;fill(array, length, symbol)
_fill_with:
	mov eax, [esp + 4]
	mov ecx, [esp + 8]						
	mov edx, [esp + 12]
	inc ecx
	fill_with_run:
		dec ecx
		jz fill_with_break
		mov [eax + ecx - 1], dl
	jmp fill_with_run
	fill_with_break:
ret

global _mega_mul
_mega_mul:   ; minimum 1 length string as argument is required
	push ebp
	push esi
	push edi
	push ebx
	mov esi, [esp + 4 + 16]
	mov ebx, [esp + 8 + 16]
	
	push multiplier
	call _fill
	add esp, 4
	
	mov eax, [size]
	mov dl, 1
	mov [multiplier + eax - 1], dl
	
	push number
	call _fill
	add esp, 4
	
	push temp_number
	call _fill
	add esp, 4
	
	mov ebp, 16
	mov edi, [size]
	mega_mul_run:
		push temp_number
		push multiplier
		call _copy_string
		add esp, 8
	
		mov al, [esi + edi - 1]
		push eax
		push temp_number
		call _mult
		
		mov eax, number
		mov [esp + 4], eax
		call _long_add
		add esp, 8
		
		push ebp
		push multiplier
		call _mult
		add esp, 8
	
		dec edi
		cmp edi, ebx
	jae mega_mul_run
	mega_mul_break:
	
	pop ebx
	pop edi
	pop esi
	pop ebp
ret
	

global _add_to_all
_add_to_all:
	mov edx, [esp + 4]
	mov eax, [esp + 8]
	mov ecx, [size]
	add_to_all_run:
		add [edx + ecx - 1], al
		dec ecx
	jnz add_to_all_run
ret

global _negate16
_negate16:
	push esi
	mov esi, [esp + 4 + 4]
	mov edx, [size]
	sub edx, 32
	mov ecx, 32
	add esi, edx
	negate16_run:
		mov al, [esi + ecx - 1]
		mov dl, 15
		sub dl, al
		mov [esi + ecx - 1], dl
		dec ecx
	jnz negate16_run

	mov ecx, esi
	add ecx, 32
	
	negate16_run2:
		dec ecx
		mov al, [ecx]
		cmp al, 15
		jnz negate16_break2
		mov al, 0
		mov [ecx], al

		cmp ecx, esi
	jnz negate16_run2
	negate16_break2:

	cmp ecx, esi
	jb negate16_small_enough
		mov al, [ecx]
		inc al
		mov [ecx], al
	negate16_small_enough:

	sub esi, edx
	pop esi
ret

global _parse_int
_parse_int:
	push ebx
	push edi
	mov esi, [esp + 4 + 8]
	xor bx, bx
	xor eax, eax
	mov cx, 10
	xor edi, edi
	width_parse_run:
		mov bl, [esi + edi]
		inc edi
		test bl, bl
			jz width_parse_break 
		sub bl, '0'
		mul cx
		add ax, bx
	jmp width_parse_run
	width_parse_break:
	pop edi
	pop ebx
ret

global _print    			
_print: 
	push esi
	push edi 
	push ebx  ;print flags

	mov esi, [esp + 12 + 12]
	mov edi, [esp + 4 + 12]

	push edi
	call _fill
	add esp, 4

	xor bx, bx
	mov al, [esi]
	cmp al, '-'
	jnz skip_get_sign
		xor bl, 1
		inc esi
	skip_get_sign:

	mov ecx, -1
	find_length:
		inc ecx
		mov al, [esi + ecx]
		test al, al
	jne find_length

	mov edx, [size]
	mov eax, 100
	mov [edi + edx], eax
	sub edx, ecx
	add edi, edx
	xor ecx, ecx
	
	convert_16char_run:
		mov al, [esi + ecx]
		cmp al, 60
		ja convert_16char_run_letter
			test al, al
			jz convert_16char_run_break
				sub al, '0'
			jmp convert_16char_run_endif
		convert_16char_run_letter:
			cmp al, 95
			jb convert_16char_run_big_letter
				sub al, 87
			jmp convert_16char_run_endif
			convert_16char_run_big_letter:
				sub al, 55
		convert_16char_run_endif:
		mov [edi + ecx], al
		inc ecx
	jmp convert_16char_run
	convert_16char_run_break:
	sub edi, edx
	
	mov eax, [size]
	sub eax, 32
	mov al, [edi + eax]
	cmp al, 8
	jb skip_check_signed
		xor bl, 1
		push edx
		push edi
		call _negate16
		add esp, 4
		pop edx
	skip_check_signed:
	
	push edx
	push edi
	call _mega_mul
	add esp, 8
	
	push edi
	push number
	call _copy_string
	add esp, 8
	
	mov esi, [esp + 8 + 12]
	xor ecx, ecx
	flags_parse_run:
		mov al, [esi + ecx]
		inc ecx
		test al, al
			jz flags_parse_break 
		cmp al, '-'
			jz flags_parse_case_minus
		cmp al, '+'
			jz flags_parse_case_plus
		cmp al, ' '
			jz flags_parse_case_space
		cmp al, '0'
			jz flags_parse_case_zero
		jmp flags_parse_break
		
		flags_parse_case_minus:
			or bl, 16
			jmp flags_parse_run
		flags_parse_case_plus:
			or bl, 4
			jmp flags_parse_run
		flags_parse_case_space:
			or bl, 2
			jmp flags_parse_run
		flags_parse_case_zero:
			or bl, 8
			jmp flags_parse_run
	jmp flags_parse_run
	flags_parse_break:
	
	mov eax, esi
	add eax, ecx
	dec eax
	push eax
	call _parse_int
	add esp, 4
	
	mov ecx, -1
	new_length_run:
		inc ecx
		mov dl, [edi + ecx]
		test dl, dl
	jz new_length_run
	
	mov edx, [size]
	cmp ecx, edx
	jne skip_if_zero
		dec ecx
	skip_if_zero:
	mov edx, [size]
	sub edx, ecx
	mov esi, edi                
	add esi, ecx                ;esi now points to start of 10-number in output
	
	mov ecx, [size]
	add ecx, edi
	num10_to_char_run:
		dec ecx
		mov bh, [ecx]
		add bh, '0'
		mov [ecx], bh
		cmp ecx, esi
	jnz num10_to_char_run
	
	test edx, edx               ;check if zero
;	jnz skip_zero_new_length
;		mov edx, 1
;	skip_zero_new_length:
	test bl, 7                  ;check has symbol before
	jz skip_has_symbol_before
		mov ecx, 7
		and cl, bl
		mov cl, [char_before_table + ecx]
		test bl, 8
		jnz some_sign_at_start
			inc edx
			dec esi
			mov [esi], cl
		jmp skip_has_symbol_before
		some_sign_at_start:
			mov [edi], cl
			inc edi
			dec eax 
	skip_has_symbol_before:
	cmp eax, edx
	jg skip_width_flag_inactive ;check whether width flag useless
		mov eax, edx
	skip_width_flag_inactive:
	mov bh, ' '
	test bl, 8
	jz skip_check_spaces_align
		mov bh, '0'
	skip_check_spaces_align:
	
	mov [temp_vars + 0], eax
	mov [temp_vars + 4], edx
	sub eax, edx
	mov [temp_vars + 8], eax
	; temp_vars: width, real length, width - real length
	; bl - flags: -, 0, +, ' ', is_neg (from mid-high bit to low)
	; bh - aligning symbol
	
	test bl, 16 
	jz right_alignment
		mov edx, [temp_vars + 4]
		push edx
		push edi
		push esi
		call _copy_small_string
		add esp, 12
	
		mov edx, [temp_vars + 4]
		add edi, edx
		xor edx, edx
		mov dl, ' '
		mov eax, [temp_vars + 8]
		push edx
		push eax
		push edi
		call _fill_with
		add esp, 12
		mov edx, [temp_vars + 4]
		sub edi, edx
	jmp case_alignment_break
	right_alignment:
		xor edx, edx
		mov dl, bh
		push edx
		push eax
		push edi
		call _fill_with
		add esp, 12

		mov eax, [temp_vars + 8]
		mov edx, [temp_vars + 4]
		add edi, eax
		push edx
		push edi
		push esi
		call _copy_small_string
		add esp, 12
		mov eax, [temp_vars + 8]
		sub edi, eax
	case_alignment_break:
	mov eax, [temp_vars + 0]
	xor edx, edx
	mov [edi + eax], edx
	
	pop ebx
	pop edi
	pop esi
ret

	section .data
multiplier: times 42 db 0
number: times 42 db 0
temp_number: times 42 db 0
size: dd 40    ; must be multiple of 4
char_before_table: db "$- -+-+-"
temp_vars: times 5 dd 0
	end

